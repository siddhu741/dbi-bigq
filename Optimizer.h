#ifndef QUERY_H
#define QUERY_H_
#include "Function.h"
#include "Statistics.h"
#include "ComparisonEngine.h"
#include "Record.h"
#include "Pipe.h" 
#include "RelOp.h"
#include "DBFile.h"
#include "ParseTree.h"
#include "QueryNode.h"
#include <map>
#include <unordered_map>
#include <iostream>
#include <vector>
#include <mutex>
#include <string.h>
struct Node;

class Optimizer{
    Statistics *s;
    int pipeSelect = 0;
	int booleanDistinctAttribute; // 1 if there is a DISTINCT in a non-aggregate query 
	int booleanDistinctFunction;  // 1 if there is a DISTINCT in an aggregate query
    std::mutex mtx;
    std::vector<RelationalOp *> operators;
    unordered_map<int,Pipe *> pipe;
    unordered_map<string, AndList *> getSelectors(vector<AndList *> list);
    void JoinsAndSelects(vector<AndList*> &joins, vector<AndList*> &selects,vector<AndList*> &selAboveJoin); 
    Function *getFunctionFromSchema(Schema *schema);
    OrderMaker *getOrderMakerFromSchema(Schema *schema);
    unordered_map<string, AndList*>processSelects(vector<AndList*> selects);
    vector<AndList*> processJoins(vector<AndList*> joins);
    struct FuncOperator * finalFunction; 
	struct NameList * groupAttributes; 
	struct NameList * attributesSelected; 
    struct TableList * tables;   
	struct AndList *cnfAndList;
    public:
        Optimizer(struct FuncOperator *finalFunction,
                struct TableList *tables,
                struct AndList * boolean,
                struct NameList * pGrpAtts,
                struct NameList * pAttsToSelect,
                int distinct_atts, 
                int distinct_func, 
                Statistics *s,
                string dir,
                string tpch,
                string catalog);
        void ExecuteQuery();
        void PrintQuery();
        Node *nodeRoot;//this is the main Node to be used later 
        Optimizer(){};

        //ToDo: change the below
        bool DropTable(string catalog_path,string dir,string name);
        bool CreateQuery(string catalog_path,string dir,CreateTable *create);
        bool InsertQuery(string catalog_path,string dir,string tpch_dir,InsertFile *insert);
        ~Optimizer() {
        delete finalFunction;
        delete tables;
        delete nodeRoot;
        delete cnfAndList;
        delete groupAttributes;
        delete attributesSelected;
        delete s;
        }
};

#endif