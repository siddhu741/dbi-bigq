#ifndef OPTIMIZER_H_
#define OPTIMIZER_H_
#include <unordered_map>
#include "Pipe.h" 
#include "RelOp.h"
#include "Function.h"
#include "Statistics.h"
#include "ComparisonEngine.h"
#include "Record.h"
#include <iostream>
#include "DBFile.h"
#include <vector>
#include "ParseTree.h"
#define PIPE_SIZE 100


enum QueryType{
    SELECTF,SELECTP,SUM,DISTINCT,JOIN,PROJECT,GROUPBY,WRITEOUT
};

static std::unordered_map<int,Pipe *> map_pipe;
static int dbNum=0;
static std::unordered_map<int,DBFile *> dbs;
static std::vector<RelationalOp *> operators;
/**
 * @brief 
 * Structure for General Node to handle recursive optimization routine to generate Query
 */
struct Node{
    CNF *cnf;
	Record *literal;
	Schema *outputSchema;
    Node *left=NULL,*right=NULL,*parent=NULL;
	Function *function;
    OrderMaker *order;
    int lPipe,rPipe,oPipe,numAttsOutput,numAttsInput;
    int *keepMe;
    DBFile *db;
    Pipe *outPipe;
    string dbfilePath;
    virtual void Execute();
    virtual void Print();
    virtual void wait();
    Node():cnf(new CNF()),literal(new Record()){};
    ~Node(){
    	delete cnf;
        delete right;
	    delete literal;
    }
};

/**
 * @brief 
 * Structure for Select File Type Node
 */
struct SelectFNode:public Node{
    SelectFile *sf=new SelectFile();
    void Execute() override;
    void Print() override;
    void wait() override;
};

/**
 * @brief 
 * Structure for Select Pipe Type Node
 */
struct SelectPNode:public Node{
    SelectPipe *selectPipe = new SelectPipe();
    void Execute() override;
    void Print() override;
    void wait() override;
};

/**
 * @brief 
 * Structure for Select Project Type Node
 */
struct ProjectNode:public Node{
    Project *project = new Project();
    void Execute() override;
    void Print() override;
    void wait() override;
}; 

/**
 * @brief 
 * Structure for Join Type Node
 */
struct JoinNode:public Node{
     Join *join = new Join;
     void Execute() override;
    void Print() override;
    void wait() override;
};

/**
 * @brief 
 * Structure for Sum Type Node
 */
struct SumNode:public Node{
    Sum *sum = new Sum;
    void Execute() override;
    void Print() override;
    void wait() override;
};

struct GroupByNode:public Node{
    GroupBy *groupBy = new GroupBy;
    void Execute() override;
    void Print() override;
    void wait() override;
    ~GroupByNode(){
        delete groupBy;
    }
};

/**
 * @brief 
 * Structure for Distinct Type Node
 */
struct DistinctNode:public Node{
    DuplicateRemoval *dr = new DuplicateRemoval;
    void Execute() override;
    void Print() override;
    void wait() override;
    ~DistinctNode(){
        delete dr;
    }
};

/**
 * @brief 
 * Structure for WriteOut Type Node
 */
struct WriteOutNode:public Node{
     WriteOut *wo = new WriteOut();
     void Execute() override;
    void Print() override;
    void wait() override;
    FILE *fp;
};

#endif