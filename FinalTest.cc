#include <iostream>
#include <gtest/gtest.h>
#include "ParseTree.h"
#include "Optimizer.h"
#include "Statistics.h"
#include "test42.h"


extern "C" {
	int yyparse(void);   // defined in y.tab.c
	int yyfuncparse(void);   // defined in yyfunc.tab.c
}

extern struct CreateTable *createTable;
extern struct InsertFile *insertFile;
extern char *dropTableName;
extern char *setOutPut;
extern	FuncOperator *finalFunction;
extern struct TableList *tables; 
extern struct AndList *boolean;
extern struct   NameList   *groupingAtts;
extern struct   NameList   *attsToSelect; 
extern int distinctAtts;
extern int distinctFunc;
extern int quit;
extern char *dropTableName;
extern char type;


using namespace std;

TEST(FinalTest, createTable){
    setup();
   
    char* cnf = "CREATE TABLE gtest_table (r_regionkey INTEGER, r_name STRING, r_comment STRING ) AS HEAP;";
    yy_scan_string(cnf);
    yyparse();

    Optimizer *q = new Optimizer();
    cout << createTable->tableName << "----------------------" <<  endl;
    if(q->CreateQuery(catalog_path,dbfile_dir,createTable)) {
        cout <<"Created table"<<createTable->tableName<<endl;
    }
    FILE* f = fopen("gtest_table.bin", "r");
    ASSERT_TRUE(f != NULL);
}

TEST(FinalTest, dropTable){
    setup();
    Statistics *statistics=new Statistics();
	statistics->LoadAllStatistics();
   
    char* cnf = "DROP TABLE gtest_table;";
    yy_scan_string(cnf);
    yyparse();

     Optimizer *q = new Optimizer();

    if(q->DropTable(catalog_path, dbfile_dir, dropTableName)) {
        std::cout<<"We have deleted the table "<<dropTableName<<" successfully"<<endl;
    }
    FILE* f = fopen("gtest_table.bin", "r");
    ASSERT_TRUE(f == NULL);
}

int main(int argc, char **argv)
{
    testing::InitGoogleTest(&argc, argv);
    return RUN_ALL_TESTS();
}