#include "QueryNode.h"



void SelectFNode::Print(){
    if(left!=nullptr)
        left->Print();
	cout <<"*****************"<<endl;
	cout <<"SelectFromFile Operation"<<endl;
	cout <<"Input File:	"<<dbfilePath<<endl;
	cout <<"Output Pipe: "<<oPipe<<endl;
	cout <<"Output Schema: " <<endl;
    outputSchema->Print();
	cout <<"Select CNF: " <<endl;
	cout <<"\t"; cnf->Print();
	cout <<"\n\n";
	if(right)
		right->Print();
}

void SelectPNode::Print(){
	if(left)
		left->Print();
	cout <<"*****************"<<endl;
	cout <<"SelectFromPipe Operation"<<endl;
	cout <<"Input Pipe:	"<<lPipe<<endl;
	cout <<"Output Pipe: "<<oPipe<<endl;
	cout <<"Output Schema: " <<endl;
    outputSchema->Print();
	cout <<"Select CNF: " <<endl;
	cout <<"\t"; cnf->Print();
	cout <<"\n\n";
	if(right)
		right->Print();
}

void ProjectNode::Print(){
	if(left)
		left->Print();
	cout <<"*****************"<<endl;
	cout <<"Project Operation"<<endl;
	cout <<"Input Pipe:	"<<lPipe<<endl;
	cout <<"Output Pipe: "<<oPipe<<endl;
	cout <<"Output Schema: " <<endl;
    outputSchema->Print();
	cout <<"Attributes to keep: "<<endl;
	cout <<"\t";
	for(int i=0;i<numAttsOutput;i++) {
		cout <<keepMe[i] <<", ";
	}
	cout <<endl;
	cout <<"\n";
	if(right)
		right->Print();
}

void JoinNode::Print(){
	if(left)
		left->Print();
	cout <<"*****************"<<endl;
	cout <<"Join Operation"<<endl;
	cout <<"Left Input Pipe: "<<lPipe<<endl;
	cout <<"Right Input Pipe: "<<rPipe<<endl;
	cout <<"Output Pipe: "<<oPipe<<endl;
	cout <<"Output Schema: " <<endl;
    outputSchema->Print();
	cout <<"Select CNF: " <<endl;
	cout <<"\t"; cnf->Print();
	cout <<"\n\n";
	if(right)
		right->Print();
}

void SumNode::Print(){
	if(left)
		left->Print();
		cout <<"*****************"<<endl;
	cout <<"Sum Operation"<<endl;
	cout <<"Input Pipe:	"<<lPipe<<endl;
	cout <<"Output Pipe: "<<oPipe<<endl;
	cout <<"Output Schema: " <<endl;
    outputSchema->Print();
	cout <<"Sum Function: " <<endl;
		function->Print();
	cout <<endl;
	cout <<"\n";
	if(right)
		right->Print();
}

void GroupByNode::Print(){
	if(left)
		left->Print();
	cout <<"*****************"<<endl;
	cout <<"GroupBy Operation"<<endl;
	cout <<"Input Pipe:	"<<lPipe<<endl;
	cout <<"Output Pipe: "<<oPipe<<endl;
	cout <<"Output Schema: " <<endl;
    outputSchema->Print();
	cout <<"Group By OrderMaker: " <<endl;
	order->Print();
	cout <<endl;
	cout <<"Group By Function: " <<endl;
	function->Print();
	cout <<endl;
	cout <<"\n";
	if(right)
		right->Print();
}

void DistinctNode::Print(){
	if(left)
		left->Print();
	cout <<"*****************"<<endl;
	cout <<"Duplicate Removal Operation"<<endl;
	cout <<"Input Pipe:	"<<lPipe<<endl;
	cout <<"Output Pipe: "<<oPipe<<endl;
	cout <<"Output Schema: " <<endl;
    outputSchema->Print();
	cout <<"\n";
	if(right)
		right->Print();
}

void WriteOutNode::Print(){
	if(left)
		left->Print();
	cout <<"*****************"<<endl;
	cout <<"Write Out"<<endl;
	cout <<"Input Pipe:	"<<lPipe<<endl;
	cout <<"Output Schema: " <<endl;
    outputSchema->Print();
	cout <<"\n";
	if(right)
		right->Print();
}

void Node::Print(){
    if(left)
        left->Print();
    if(right)
        right->Print();
}


void Node::Execute(){
}
/**
 * @brief This method helps in Executing Select File Node. It create the DBFIle instance of specified file.
 */
void SelectFNode::Execute(){
	if(left!=NULL)
		left->Execute();
	if(right!=NULL)
		right->Execute();
	#ifdef F_DEBUG
		cout <<"execute selectfrom file: " <<dbfilePath<<endl;
	#endif
	Pipe *sfOutPipe = new Pipe(PIPE_SIZE);
	map_pipe[oPipe] = sfOutPipe;
	DBFile *db=new DBFile();
	db->Open((char*)(dbfilePath + ".bin").c_str());
	sf->Run(*db, *sfOutPipe, *(cnf), *(literal));
	if(left)
		left->wait();
	if(right)
		right->wait();
}
/**
 * @brief Wait till Select file thread execution is complete.
 */
void SelectFNode::wait(){
	sf->WaitUntilDone();
}
/**
 * @brief This method helps in Executing Select Pipe Node. It loads the records in Pipe.
 */
void SelectPNode::Execute(){
	if(left)
		left->Execute();
	if(right)
		right->Execute();
	//selectPipe->Use_n_Pages(RUNLEN);
	Pipe *spo = new Pipe(PIPE_SIZE);
	//add it to the pipe
	map_pipe[oPipe] = spo;
	Pipe *spl = map_pipe[lPipe];
	selectPipe->Run(*spl, *spo, *(cnf), *(literal));
	//operators.push_back(selectPipe);
	if(left)
		left->wait();
	if(right)
		right->wait();
}
/**
 * @brief Wait till Select Pipe thread execution is complete.
 */
void SelectPNode::wait(){
	selectPipe->WaitUntilDone();
}

void Node::wait(){
	
}
/**
 * @brief This method helps in Executing ProjectNode. It Takes the required output from input pipe and moves it to output pipe.
 */
void ProjectNode::Execute(){
	if(left)
		left->Execute();
	if(right)
		right->Execute();
	#ifdef F_DEBUG
		std::cout<<"In Project Node\n";
	#endif
	Pipe *pOutPipe = new Pipe(PIPE_SIZE);
	//add it to the pipe
	map_pipe[oPipe] = pOutPipe;
	Pipe *plPipe = map_pipe[lPipe];
	project->Run(*plPipe, *pOutPipe, keepMe, numAttsInput, numAttsOutput);
	if(left)
		left->wait();
	if(right)
		right->wait();
	//operators.push_back(project);
}
/**
 * @brief Wait till Project Node thread execution is complete.
 */
void ProjectNode::wait(){
	project->WaitUntilDone();
}
/**
 * @brief This method helps in Executing Join Node. It performs Join operation.
 */
void JoinNode::Execute(){
	if(left)
		left->Execute();
	if(right)
		right->Execute();
	Pipe *jOutPipe = new Pipe(PIPE_SIZE);
	//add it to the pipe
	map_pipe[oPipe] = jOutPipe;
	Pipe *jlPipe = map_pipe[lPipe];
	Pipe *jrPipe = map_pipe[rPipe];
	join->Run(*jlPipe, *jrPipe, *jOutPipe, *(cnf), *(literal));
	//operators.push_back(join);
	if(left)
		left->wait();
	if(right)
		right->wait();
}
/**
 * @brief Wait till JoinNode thread execution is complete.
 */
void JoinNode::wait(){
	join->WaitUntilDone();
}
/**
 * @brief This method helps in Executing Sum Node. It helps in computing sum of records.
 */
void SumNode::Execute(){
	if(left)
		left->Execute();
	if(right)
		right->Execute();
	Pipe *sOutPipe = new Pipe(PIPE_SIZE);
	map_pipe[oPipe] = sOutPipe;
	Pipe *slPipe = map_pipe[lPipe];
	sum->Run(*slPipe, *sOutPipe, *(function));
	//operators.push_back(sum);
	if(left)
		left->wait();
	if(right)
		right->wait();
}
/**
 * @brief Wait till SumNode thread execution is complete.
 */
void SumNode::wait(){
	sum->WaitUntilDone();
}
/**
 * @brief This method helps in Executing GroupBy Node. It does group by of recods and computes sum of aggregated column.
 */
void GroupByNode::Execute(){
	if(left)
		left->Execute();
	if(right)
		right->Execute();
//		groupBy->Use_n_Pages(RUNLEN);
	Pipe *gbOutPipe = new Pipe(PIPE_SIZE);
	map_pipe[oPipe] = gbOutPipe;
	Pipe *gblPipe = map_pipe[lPipe];
	groupBy->Run(*gblPipe, *gbOutPipe, *(order), *(function));
	if(left)
		left->wait();
	if(right)
		right->wait();
	//operators.push_back(groupBy);
}
/**
 * @brief Wait till GroupBy Node thread execution is complete.
 */
void GroupByNode::wait(){
	groupBy->WaitUntilDone();
}
/**
 * @brief This method helps in Executing Distinct Node. It helps in comuting Distinct value.
 */
void DistinctNode::Execute(){
	if(left)
		left->Execute();
	if(right)
		right->Execute();
//		dr->Use_n_Pages(RUNLEN);
	Pipe *drOutPipe = new Pipe(PIPE_SIZE);
	map_pipe[oPipe] = drOutPipe;
	Pipe *drlPipe = map_pipe[lPipe];
	dr->Run(*drlPipe, *drOutPipe, *(left->outputSchema));
	if(left)
		left->wait();
	if(right)
		right->wait();
}
/**
 * @brief Wait till Distinct Node thread execution is complete.
 */
void DistinctNode::wait(){
	dr->WaitUntilDone();
}
/**
 * @brief This method helps in Executing WriteOut Node. It helps in printing output to file or command line.
 */
void WriteOutNode::Execute(){
	if(left)
		left->Execute();
	if(right)
		right->Execute();
	std::fstream str("data");
	if(!str){
		Print();
		return ;
	}
	string choice;
	str>>choice;
	if(choice=="NONE"){
		Print();
		return;
	}
	if(choice=="STDOUT"){
		fp=nullptr;
	}
	else{
		fp=fopen(choice.c_str(),"w");
	}
	Pipe *wlPipe = map_pipe[lPipe];
	wo->Run(*wlPipe, fp, *(outputSchema));
	//cout <<"total pipe size: " <<pipe.size()<<endl;
	if(left)
		left->wait();
	if(right)
		right->wait();
}
/**
 * @brief Wait till WriteOut Node thread execution is complete.
 */
void WriteOutNode::wait(){
	wo->WaitUntilDone();
}

