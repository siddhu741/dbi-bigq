#ifndef BIGQ_H
#define BIGQ_H
#include <pthread.h>
#include <iostream>
#include "Pipe.h"
#include<vector>
#include "File.h"
#include "Record.h"
#include<algorithm>
#include<queue>

using namespace std;

void *processBigQ(void*);
int GetRecSize (Record* r);

class BigQ {

        friend void *processBigQ(void *);
        friend bool compareRecordsFunc(Record * left, Record * right);
        public:
                BigQ (Pipe &inputPipe, Pipe &outputPipe, OrderMaker &sortOrder, int runLength);
                void mergeSortedRuns(vector < pair < int, int > > pageIndexesForRun, Pipe & out, char * fileNameStr, OrderMaker sortOrder);
                ~BigQ();
        private:
                Pipe& inputPipe;
                Pipe& outputPipe;
                OrderMaker& sortOrder;
                int runLength;
                pthread_t workerThread;
};

#endif