/**
 * @brief This class helps calculate statistical information of databse's attributes and relations
 * and store them in a file Statistics.txt.
 */
#include "Statistics.h"
Statistics::Statistics()
{
}
Statistics::Statistics(Statistics &copyMe)
{
}
Statistics::~Statistics()
{
}
/**
 * This method help adds relation
 * @param relName Name of Relations
 * @param numTuples Number of relation tuples.
 */
void Statistics::AddRel(char *relName, int numTuples)
{
    mapRelation[string(relName)].numAttr = numTuples;
    mapEstimate[string(relName)] = numTuples;
}
/**
 * This method help adds attributes to the base relation structure.
 * @param relName Name of Relations
 * @param attName Name of Attribute
 * @param numDistincts Number of distinct attributes.
 */
void Statistics::AddAtt(char *relName, char *attName, int numDistincts)
{
    mapRelation[string(relName)].AddAttribute(string(attName), numDistincts);
}
/**
 * This method help copy relation and store as a new name
 * @param oldNmae Name of old Relations
 * @param newName New Relation name to copy old relation
 */
void Statistics::CopyRel(char *oldName, char *newName)
{
    mapRelation[string(newName)].numAttr = mapRelation[string(oldName)].numAttr;
    for (auto iter : mapRelation[string(oldName)].getMapAttribute())
    {
        mapRelation[string(newName)].getMapAttribute()[string(newName) + "." + iter.first] = iter.second;
    }
    mapEstimate[string(newName)] = mapRelation[string(newName)].numAttr;
}
/**
 * This method help read Statistics information from a Statistics file Statistics.txt
 * @param fromWhere Location to read Statistics file from
 */
void Statistics::Read(char *fromWhere)
{
    ifstream in(fromWhere);
    in >> *this;
    in.close();
}
/**
 * This method help write Statistics infromation to a file Statistics.txt
 * @param fromWhere Location to store Statistics file
 */
void Statistics::Write(char *fromWhere)
{
    ofstream out(fromWhere);
    out << *this;
    out.close();
}
/**
 * This Apply operation uses the statistics stored to simulate a join of 
 * all the relations listed in the relNames parameter.
 * @param parseTree List of selection predicate of Relations, should only contain attributes already added.
 * @param relNames List of all relation names
 * @param numToJoin Number of Join
 */
void Statistics::Apply(struct AndList *parseTree, char *relNames[], int numToJoin){
    if (validateRelation(relNames, numToJoin)){
        double fact = 1.0;
        double orFact = 0;
        double orFactDe = 0;
        double orFactPartial = 0;
        struct AndList *pA = parseTree;
        long double maxTup = 1;
        bool reltable[numToJoin];
        string newSet;
        
        while (pA != NULL){
            struct OrList *pOr = pA->left;
            orFact = 0;
            orFactDe = 1;
            unordered_map<string, int> diff;
            string previous;
            while (pOr != NULL){
                double tLeft = 1.0;
                double tRight = 1.0;
                struct ComparisonOp *pComp = pOr->left;
                if (!validateAttributes(pComp->right, tRight, relNames, numToJoin) || !validateAttributes(pComp->left, tLeft, relNames, numToJoin)){
                    return;
                }
                string oper1(pComp->left->value);

                if (pComp->code == 1 || pComp->code == 2){
                    orFact = orFact + (1.0/3);
                    orFactDe = orFactDe * (1.0/3);
                    for (auto &iter : mapRelation){
                        if (iter.second.getMapAttribute().count(oper1) != 0){
                            iter.second.getMapAttribute()[oper1] = iter.second.getMapAttribute()[oper1] / 3;
                        }
                    }
                    pOr = pOr->rightOr;
                    continue;
                }
                if (tLeft < tRight || tRight == 1.0 || tLeft > tRight){
                    if (diff.count(oper1)){
                        diff[oper1]++;
                    }
                    else{
                        diff[oper1] = min(tLeft, tRight);
                    }
                }
                if (oper1.compare(previous) != 0 && orFactPartial != 0){
                    orFact = orFact + orFactPartial;
                    orFactDe = orFactDe * orFactPartial;
                    orFactPartial = 0;
                }
                orFactPartial = orFactPartial + (1.0 / max(tLeft, tRight));
                pOr = pOr->rightOr;
                previous = oper1;
            }

            if (orFactPartial){
                orFact = orFact + orFactPartial;
                orFactDe = orFactDe * orFactPartial;
                orFactPartial = 0;
            }

            if (orFact == orFactDe){
                fact = fact * orFact;
            }
            else{
                fact = fact * (orFact - orFactDe);
            }

            for (auto &ch : diff){
                for (auto &rel : mapRelation){
                    if (rel.second.getMapAttribute().count(ch.first)){
                        rel.second.getMapAttribute()[ch.first] = ch.second;
                    }
                }
            }
            pA = pA->rightAnd;
        }

        int i = 0;
        while (i<numToJoin){
            reltable[i] = true;
            i = i + 1;
        }

        i = 0;
        while (i<numToJoin){
            if (reltable[i]){
                string relname(relNames[i]);
                auto iter = mapEstimate.begin();
                while (iter !=  mapEstimate.end()){
                    auto check = split(iter->first);
                    if (check.count(relNames[i]) != 0){
                        reltable[i] = false;
                        maxTup *= iter->second;
                        int j = i + 1;
                        while (j < numToJoin){
                            if (check.count(relNames[j]) != 0){
                                reltable[j] = false;
                            }
                            j = j + 1;
                        }
                        break;
                    }
                    iter++;
                }
            }
            i = i + 1;
        }

        double result = fact * maxTup;

        i = 0;
        while (i<numToJoin){
            string relname(relNames[i]);
            newSet = newSet + relname + "#";
            for (auto iter = mapEstimate.begin(); iter != mapEstimate.end(); iter++){
                auto check = split(iter->first);
                if (check.count(relname) != 0){
                    mapEstimate.erase(iter);
                    break;
                }
            }
            i = i + 1;
        }
        mapEstimate.insert({newSet, result});
    }
}
/**
 * This operation computes the number of tuples that would result from a join over the relations in relNames
 * @param parseTree List of selection predicate of Relations, should only contain attributes already added.
 * @param relNames List of all relation names
 * @param numToJoin Number of Join
 * @return Estimate value of number of tuples.
 */
double Statistics::Estimate(struct AndList *parseTree, char **relNames, int numToJoin)
{
    if (validateRelation(relNames, numToJoin)){
        struct AndList *pA = parseTree;
        double fact = 1.0, orFact = 0;
        double orFactDe = 0, orFactPartial = 0;
        long double maxTup = 1;
        vector<bool> reltable(numToJoin, true);
        while (pA != NULL){
            struct OrList *pOr = pA->left;
            orFact = 0;
            orFactDe = 1;
            string previous;
            while (pOr != NULL){
                struct ComparisonOp *pComp = pOr->left;
                double tLeft = 1.0, tRight = 1.0;
                if (!validateAttributes(pComp->right, tRight, relNames, numToJoin) || !validateAttributes(pComp->left, tLeft, relNames, numToJoin)){
                    return 0;
                }
                if (pComp->code == 1 || pComp->code == 2){
                    orFactDe = orFactDe * (1.0 / 3);
                    orFact = orFact + (1.0 / 3);
                    pOr = pOr->rightOr;
                    continue;
                }

                string oper1(pComp->left->value);
                if (oper1.compare(previous) != 0 && orFactPartial != 0){
                    orFact = orFact + orFactPartial;
                    orFactDe = orFactDe * orFactPartial;
                    orFactPartial = 0;
                }
                orFactPartial =  orFactPartial + (1.0 / max(tLeft, tRight));
                previous = oper1;
                pOr = pOr->rightOr;
            }
            if (orFactPartial != 0){
                orFact = orFact + orFactPartial;
                orFactDe = orFactDe * orFactPartial;
                orFactPartial = 0;
            }
            if (orFact == orFactDe){
                fact *= orFact;
            }else{
                fact *= (orFact - orFactDe);
            }
            pA = pA->rightAnd;
        }

        int i = 0;
        while (i<numToJoin){
            if (reltable[i]){
                string relname(relNames[i]);
                auto iter = mapEstimate.begin();
                while (iter != mapEstimate.end()){
                    auto check = split(iter->first);
                    if (check.count(relNames[i])){
                        reltable[i] = false;
                        maxTup *= iter->second;
                        int j = 0;
                        while (j < numToJoin){
                            if (check.count(relNames[j])){
                                reltable[j] = false;
                            }
                            j = j + 1;
                        }
                        break;
                    }
                    iter ++;
                }
            }
            i = i + 1;
        }
        return fact * maxTup;
    }else{
        return 0;
    }
}

bool Statistics::validateAttributes(struct Operand *leftOperand, double &oprandRelation, char **relNames, int numToJoin)
{
    string operand(leftOperand->value);
    if (leftOperand->code == 4)
    {
        bool isAvailable = false;
        for (int i = 0; i < numToJoin; i++)
        {
            string relname(relNames[i]);
            if (mapRelation[relname].mapAttirbute.count(operand))
            {
                isAvailable = true;
                oprandRelation = mapRelation[relname].mapAttirbute[operand] != -1 ? mapRelation[relname].mapAttirbute[operand] : oprandRelation;
                return isAvailable;
            }
        }
        cout << operand << " not Available.\n";
        return false;            
    }
    return true;
}
bool Statistics::validateRelation(char *relName[], int numJoin)
{
    unordered_set<string> setRelation;
    for (int i = 0; i < numJoin; i++)
    {
        setRelation.insert(string(relName[i]));
    }
    for (int i = 0; i < numJoin; i++)
    {
        string rel(relName[i]);
        for (auto iter : mapEstimate)
        {
            auto check = split(iter.first);
            if (check.count(rel))
            {
                for (auto i : check)
                {
                    if (!setRelation.count(i))
                    {
                        return false;
                    }
                }
            }
        }
    }
    return true;
}


std::ostream &operator<<(std::ostream &os, const RelationStruct &oRelationStruct)
{
    os << oRelationStruct.numAttr << " " << oRelationStruct.mapAttirbute.size() << " ";
    for (auto i : oRelationStruct.mapAttirbute)
    {
        os << i.first << " " << i.second << " ";
    }
    return os;
}

std::istream &operator>>(std::istream &is, RelationStruct &oRelationStruct)
{
    int size;
    is>>oRelationStruct.numAttr>>size;
    for(int i=0;i<size;i++){
        string temp;
        is>>temp;
        is>>oRelationStruct.mapAttirbute[temp];
    }
    return is;
}

std::ostream &operator<<(std::ostream &os, const Statistics &stat)
{
    os << stat.mapRelation.size() << " ";
    std::stringstream oStringStream;
    for (auto iter : stat.mapRelation)
    {
        os << iter.first << " " << iter.second;
    }
    oStringStream<<stat.mapEstimate.size()<<" ";
    for(auto it:stat.mapEstimate) {
        oStringStream<<(it).first<<" "<<it.second<<" ";
    }
    os << oStringStream.str();
    return os;


    // os << "mapRelation size: " << stat.mapRelation.size() << endl << endl;
    // std::stringstream oStringStream;
    // int counter = 1;
    // for (auto iter : stat.mapRelation)
    // {
    //     os << iter.first << " " << iter.second << endl;
    // }
    // // cout << "----" << endl;

    // oStringStream << "mapEstimate size: " << stat.mapEstimate.size() << endl << endl;
    // for(auto it:stat.mapEstimate) {
    //     oStringStream<<(it).first<<" "<<it.second<<endl;
    // }
    // os << oStringStream.str() << endl;
    // return os;
}

std::istream &operator>>(std::istream &is, Statistics &stat)
{
   int size;

    is >> size;
    for (int i = 0; i < size; i++)
    {
        string input;
        struct RelationStruct oRelationStruct;
        is >> input >> oRelationStruct;
        stat.mapRelation[input] = oRelationStruct;
    }
    is >> size;
    for (int i = 0; i < size; i++)
    {
        string input;
        double dbl;
        is >> input >> dbl;
        stat.mapEstimate[input] = dbl;
    }
    return is; 
    
    // int size;

    // is >> size;
    // for (int i = 0; i < size; i++)
    // {
    //     //ToDo: Update the code to read and write in format as write
    // }
    // is >> size;
    // for (int i = 0; i < size; i++)
    // {
    //     //ToDo: Update the code to read and write in format as write
    // }
    // return is;
}

int Statistics::ParseRelation(string name, string &rel) {
	int prefixPos = name.find(".");
	if(prefixPos != string::npos) {
		rel = name.substr(0,prefixPos);
	}
	return 1;
}

void Statistics::LoadAllStatistics() {
		char *relName[] = {(char*)"supplier",(char*)"partsupp", (char*)"lineitem",
					(char*)"orders",(char*)"customer",(char*)"nation", (char*)"part", (char*)"region"};
		AddRel(relName[0],10000);     //supplier
	    AddRel(relName[1],800000);    //partsupp
	    AddRel(relName[2],6001215);   //lineitem
	    AddRel(relName[3],1500000);   //orders
	    AddRel(relName[4],150000);    //customer
	    AddRel(relName[5],25);        //nation
	    AddRel(relName[6], 200000);   //part
	    AddRel(relName[7], 5);        //region
//	    AddRel(relName[8], 3);        //mal_test


	    AddAtt(relName[0], (char*)"s_suppkey",10000);
	    AddAtt(relName[0], (char*)"s_nationkey",25);
		AddAtt(relName[0], (char*)"s_acctbal",9955);
		AddAtt(relName[0], (char*)"s_name",100000);
		AddAtt(relName[0], (char*)"s_address",100000);
		AddAtt(relName[0], (char*)"s_phone",100000);
		AddAtt(relName[0], (char*)"s_comment",10000);


	    AddAtt(relName[1], (char*)"ps_suppkey", 10000);
	    AddAtt(relName[1], (char*)"ps_partkey", 200000);
	    AddAtt(relName[1], (char*)"ps_availqty", 9999);
	    AddAtt(relName[1], (char*)"ps_supplycost", 99865);
	    AddAtt(relName[1], (char*)"ps_comment", 799123);


	    AddAtt(relName[2], (char*) "l_returnflag",3);
	    AddAtt(relName[2], (char*)"l_discount",11);
	    AddAtt(relName[2], (char*)"l_shipmode",7);
	    AddAtt(relName[2], (char*)"l_orderkey",1500000);
	    AddAtt(relName[2], (char*)"l_receiptdate",0);
	    AddAtt(relName[2], (char*)"l_partkey",200000);
	    AddAtt(relName[2], (char*)"l_suppkey",10000);
	    AddAtt(relName[2], (char*)"l_linenumbe",7);
	    AddAtt(relName[2], (char*)"l_quantity",50);
	    AddAtt(relName[2], (char*)"l_extendedprice",933900);
	    AddAtt(relName[2], (char*)"l_tax",9);
	    AddAtt(relName[2], (char*)"l_linestatus",2);
	    AddAtt(relName[2], (char*)"l_shipdate",2526);
	    AddAtt(relName[2], (char*)"l_commitdate",2466);
	    AddAtt(relName[2], (char*)"l_shipinstruct",4);
	    AddAtt(relName[2], (char*)"l_comment",4501941);


	    AddAtt(relName[3], (char*)"o_custkey",150000);
	    AddAtt(relName[3], (char*)"o_orderkey",1500000);
	    AddAtt(relName[3], (char*)"o_orderdate",2406);
	    AddAtt(relName[3], (char*)"o_totalprice",1464556);
	    AddAtt(relName[3], (char*)"o_orderstatus", 3);
	    AddAtt(relName[3], (char*)"o_orderpriority", 5);
	    AddAtt(relName[3], (char*)"o_clerk", 1000);
	    AddAtt(relName[3], (char*)"o_shippriority", 1);
	    AddAtt(relName[3], (char*)"o_comment", 1478684);


	    AddAtt(relName[4], (char*)"c_custkey",150000);
	    AddAtt(relName[4], (char*)"c_nationkey",25);
	    AddAtt(relName[4], (char*)"c_mktsegment",5);
	    AddAtt(relName[4], (char*)"c_name", 150000);
	    AddAtt(relName[4], (char*)"c_address", 150000);
	    AddAtt(relName[4], (char*)"c_phone", 150000);
	    AddAtt(relName[4], (char*)"c_acctbal", 140187);
	    AddAtt(relName[4], (char*)"c_comment", 149965);

	    AddAtt(relName[5], (char*)"n_nationkey",25);
	    AddAtt(relName[5], (char*)"n_regionkey",5);
	    AddAtt(relName[5], (char*)"n_name",25);
	    AddAtt(relName[5], (char*)"n_comment",25);

	    AddAtt(relName[6], (char*)"p_partkey",200000);
	    AddAtt(relName[6], (char*)"p_size",50);
	    AddAtt(relName[6], (char*)"p_container",40);
	    AddAtt(relName[6], (char*)"p_name", 199996);
		AddAtt(relName[6], (char*)"p_mfgr", 5);
		AddAtt(relName[6], (char*)"p_brand", 25);
		AddAtt(relName[6], (char*)"p_type", 150);
		AddAtt(relName[6], (char*)"p_retailprice", 20899);
		AddAtt(relName[6], (char*)"p_comment", 127459);


	    AddAtt(relName[7], (char*)"r_regionkey",5);
	    AddAtt(relName[7], (char*)"r_name",5);
	    AddAtt(relName[7], (char*)"r_comment",5);

}