
#include <iostream>
#include "ParseTree.h"
#include "Optimizer.h"
#include "Statistics.h"
#include "test42.h"
#include <string>
#include <chrono>
using namespace std;

extern "C" {
	int yyparse(void);   // defined in y.tab.c
	int yyfuncparse(void);   // defined in yyfunc.tab.c
	int yylex(void);
	int yywrap();
	void init_lexical_parser (char *src);
	void close_lexical_parser ();
	void yylex_destroy();
}


extern struct CreateTable *createTable;
extern struct InsertFile *insertFile;
extern char *dropTableName;
extern char *setOutPut;
extern	FuncOperator *finalFunction;
extern struct TableList *tables; 
extern struct AndList *boolean;
extern struct   NameList   *groupingAtts;
extern struct   NameList   *attsToSelect; 
extern int distinctAtts;
extern int distinctFunc;
extern int quit;
extern char *dropTableName;
extern char type;
std::mutex mtx;
int main () {
	setup();
	typedef std::chrono::high_resolution_clock Time;
    typedef std::chrono::milliseconds ms;
    typedef std::chrono::duration<float> fsec;
		std::cout<<"--------------------------------------------------\n";
		std::cout<<"You can enter one of the following commands:"<<endl;
		std::cout<<"CREATE, INSERT, SELECT, DROP, SET OUTPUT"<<endl;
		std::cout<<"You can create table using HEAP or SORTED option"<<endl;
		std::cout<<"You can set OUTPUT to None, <a file>, STDOUT"<<endl;
		std::cout<<"---------------------------------------------------\n";
		string choice;
		//std::cin>>choice;
		//init_lexical_parser(const_cast<char *>(choice.c_str()));
		yyparse();
		auto t0 = Time::now();
		mtx.lock();
		if(quit)
		{
			mtx.unlock();
			return 0;
		}
		else if(type=='c'){

			Optimizer *q = new Optimizer();
			if(q->CreateQuery(catalog_path,dbfile_dir,createTable)) {
				cout <<"TABLE is successfully created: "<<createTable->tableName<<endl;
			}
			delete q;
			type='z';
		}
		else if(type=='s'){
			ofstream ifs(string(dbfile_dir)+"data");
			ifs<<setOutPut;
			ifs.close();
		}
		else if(type=='i') {
			Optimizer *q=new Optimizer();
			std::cout <<insertFile->tableName<<"\n";
			if(q->InsertQuery(catalog_path,dbfile_dir,tpch_dir,insertFile))
				cout <<"Loaded file "<<insertFile->fileName<<" into " <<insertFile->tableName<<endl;
			delete q;
			type='z';
		}
		else if(type=='d'){
			Optimizer *q=new Optimizer();
			if(q->DropTable(catalog_path,dbfile_dir,dropTableName)){
				std::cout<<"TABLE with name "<<dropTableName<<" dropped successfully"<<endl;
			}
			delete q;
			type='z';
		}
		else if(type=='q'){
			Statistics * s=new Statistics();

			s->Read("Statistics.txt");
			Optimizer *q=new Optimizer(finalFunction,tables,boolean,groupingAtts,attsToSelect,distinctAtts,distinctFunc,s,std::string(dbfile_dir),string(tpch_dir),string(catalog_path));
			q->ExecuteQuery();
			delete q;
			type='z';
		}
		mtx.unlock();
		auto t1 = Time::now();
		fsec fs = t1 - t0;
    	ms d = std::chrono::duration_cast<ms>(fs);
    	std::cout << "Duration of the Query: " << fs.count() << "s\n";
	cleanup();
	return 1;
}	


