/**
 * @brief A Heap Implmentation of DBFIle. Creates an unordered file of records,
 * where new records simply go to the end of file. 
 * 
 * 
 */
#ifndef DBFileSorted_H
#define DBFileSorted_H

#include "TwoWayList.h"
#include "Record.h"
#include "Schema.h"
#include "Pipe.h"
#include "File.h"
#include "BigQ.h"
#include "DBFileInterface.h"
#include "Comparison.h"
#include "ComparisonEngine.h"
#include "Defs.h"

class DBFileSorted : public DBFileInterface
{
    enum rw_mode
    {
        reading,
        writing
    };
    rw_mode mode;
    string f_path;
    int runLen;
    OrderMaker oOrderMaker;
    static const int pipeBufferSize = 100;
    Pipe *in;
    Pipe *out;
    BigQ *oBigQ;
    File oFile;
    Page oPage;
    off_t pageNumber;
   

    void TwoWayMerge();
    void doMerge();

public:
    DBFileSorted();

    int Create(const char *fpath, fType file_type, void *startup);
    int Open(const char *fpath);
    int Close();

    void Load(Schema &myschema, const char *loadpath);

    void MoveFirst();
    void Add(Record &addme);
    int GetNext(Record &fetchme);
    int GetNext(Record &fetchme, CNF &cnf, Record &literal);

    int BinarySearch(Record& fetchme, OrderMaker& queryorder, Record& literal, OrderMaker& cnforder, ComparisonEngine& cmp);
};
#endif
