#include "DBFileSorted.h"
#include "DBFile.h"
#include <gtest/gtest.h>
#include <iostream>


TEST(DBFileSorted, checkTBLHasData){
	
	const char *f_path = "test.bin";
	DBFileSorted dbFile;
	int sortFileCreated = dbFile.Create(f_path, heap, NULL);
    
    ASSERT_TRUE(sortFileCreated == 1);
}



int main(int argc, char **argv)
{
    testing::InitGoogleTest(&argc, argv);
    return RUN_ALL_TESTS();
}