var searchData=
[
  ['gbarguments',['GBArguments',['../structGBArguments.html',1,'']]],
  ['getnext',['GetNext',['../classDBFileHeap.html#a59854794a3fc48d71ff41551e8b5154c',1,'DBFileHeap::GetNext(Record &amp;fetchme)'],['../classDBFileHeap.html#a2f419e062bdb3787cfd4afe779ada28d',1,'DBFileHeap::GetNext(Record &amp;fetchme, CNF &amp;cnf, Record &amp;literal)'],['../classDBFileInterface.html#aba22ec2df42e048bf6da7d5118ebe254',1,'DBFileInterface::GetNext(Record &amp;fetchme)=0'],['../classDBFileInterface.html#ace1a734b433a4ee7aa2d7f8b189b7966',1,'DBFileInterface::GetNext(Record &amp;fetchme, CNF &amp;cnf, Record &amp;literal)=0'],['../classDBFileSorted.html#a813b1f89270fe87e08d0e60f4a772ee3',1,'DBFileSorted::GetNext(Record &amp;fetchme)'],['../classDBFileSorted.html#ab10c2e8119aa7a6a24f770277abeec32',1,'DBFileSorted::GetNext(Record &amp;fetchme, CNF &amp;cnf, Record &amp;literal)']]],
  ['getrecsize',['GetRecSize',['../classRecord.html#a55d1f2b32c0d55e9a3c3c2dd4a74f6f8',1,'Record']]],
  ['groupby',['GroupBy',['../classGroupBy.html',1,'']]],
  ['groupbynode',['GroupByNode',['../structGroupByNode.html',1,'']]]
];
