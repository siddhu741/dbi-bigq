var searchData=
[
  ['close',['Close',['../classDBFileHeap.html#a5bdb09a5d4937ff5cb48133567c62f79',1,'DBFileHeap::Close()'],['../classDBFileInterface.html#aa7819e968b5c92298bb5ccf4fd321ded',1,'DBFileInterface::Close()'],['../classDBFileSorted.html#a36bb489f7adc1e2830c3e276902c836c',1,'DBFileSorted::Close()']]],
  ['cnf',['CNF',['../classCNF.html',1,'']]],
  ['comparerecords',['compareRecords',['../classcompareRecords.html',1,'']]],
  ['comparison',['Comparison',['../classComparison.html',1,'']]],
  ['comparisonengine',['ComparisonEngine',['../classComparisonEngine.html',1,'']]],
  ['comparisonop',['ComparisonOp',['../structComparisonOp.html',1,'']]],
  ['copyrel',['CopyRel',['../classStatistics.html#a1c764fe27f31deaa1f03c9895a416b5b',1,'Statistics']]],
  ['create',['Create',['../classDBFileHeap.html#a28e381d93b9d299174989e84c7156fa9',1,'DBFileHeap::Create()'],['../classDBFileInterface.html#a64a3332f4c7673b1dbfeec415c276dbd',1,'DBFileInterface::Create()'],['../classDBFileSorted.html#aaf77ca5a1eea42f361b9fc679a098577',1,'DBFileSorted::Create()']]],
  ['createquery',['CreateQuery',['../classOptimizer.html#a2db31b8011e416353439876883347967',1,'Optimizer']]],
  ['createtable',['CreateTable',['../structCreateTable.html',1,'']]]
];
