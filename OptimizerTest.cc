#include <iostream>
#include <gtest/gtest.h>
#include "ParseTree.h"
#include "Optimizer.h"
#include "Statistics.h"
#include "test42.h"


extern "C" {
	int yyparse(void);   // defined in y.tab.c
	int yyfuncparse(void);   // defined in yyfunc.tab.c
}

extern	FuncOperator *finalFunction;
extern struct TableList *tables; 
extern struct AndList *boolean;
extern struct   NameList   *groupingAtts;
extern struct   NameList   *attsToSelect; 
extern int distinctAtts;
extern int distinctFunc;


using namespace std;

TEST(OptimizerTest, SellectSQL) {
    setup();
	
	Statistics *statistics=new Statistics();
	statistics->LoadAllStatistics();
    char* cnf = "SELECT n.n_nationkey FROM nation AS n WHERE (n.n_name = 'UNITED STATES')";
    yy_scan_string(cnf);

	yyparse();
	Optimizer *obj_query = new Optimizer(finalFunction,tables,boolean,groupingAtts,attsToSelect,distinctAtts,distinctFunc,statistics,std::string(dbfile_dir),string(tpch_dir),string(catalog_path));
	obj_query->PrintQuery();
    ASSERT_EQ(obj_query->nodeRoot->oPipe, 1);
    ASSERT_EQ(obj_query->nodeRoot->lPipe, 0);
    ASSERT_EQ(obj_query->nodeRoot->rPipe, 1651781229);
	delete s, p, ps, n, li, r, o, c;
	free (catalog_path);
  //  ASSERT_EQ(obj_query, 800000);
}

TEST(OptimizerTest, JOINSQL) {
    setup();
	
	Statistics *statistics=new Statistics();
	statistics->LoadAllStatistics();

    char* cnf = "SELECT n.n_name FROM nation AS n, region AS r WHERE (n.n_regionkey = r.r_regionkey) AND (n.n_nationkey > 5)";
    yy_scan_string(cnf);

	yyparse();
	Optimizer *obj_query = new Optimizer(finalFunction,tables,boolean,groupingAtts,attsToSelect,distinctAtts,distinctFunc,statistics,std::string(dbfile_dir),string(tpch_dir),string(catalog_path));
	obj_query->PrintQuery();
    ASSERT_EQ(obj_query->nodeRoot->oPipe, 3);
    ASSERT_EQ(obj_query->nodeRoot->lPipe, 2);
    ASSERT_EQ(obj_query->nodeRoot->rPipe, 0);
	delete s, p, ps, n, li, r, o, c;
	free (catalog_path);
  //  ASSERT_EQ(result, 800000);
}


int main(int argc, char **argv)
{
    testing::InitGoogleTest(&argc, argv);
    return RUN_ALL_TESTS();
}