#include "DBFileSorted.h"
#include "iostream"
#include "fstream"
#include "TwoWayList.h"
#include "Record.h"
#include "Schema.h"
#include "File.h"
#include "Comparison.h"
#include "ComparisonEngine.h"
#include "Defs.h"
#include <string>

DBFileSorted::DBFileSorted() : mode(reading), oFile(), oPage(), pageNumber(0), runLen(50), oOrderMaker(), in(NULL), out(NULL), oBigQ(NULL)
{
}
/**
 * @brief Erases an existing file and creates a new File with Heap type record storage.
 * @param f_path path of file that needs to be created.
 * @return 1 on success and 0 on failure.
 */
int DBFileSorted::Create(const char *f_path, fType f_type, void *startup)
{
  try
  {
    oFile.Open(0, f_path);
    cout << "File created \n";
  }
  catch (...)
  {
    cerr << "DBFileSorted create error. \n";
    return 0;
  }
  return 1;
}
/** 
 * @brief Loads all record from .tbl file to Heap file.
 * @param f_schema schema of file to be loaded.
 * @param loadpath path of file to be loaded.
*/
void DBFileSorted::Load(Schema &f_schema, const char *loadpath)
{
  mode = writing;
  if (oBigQ == NULL)
  {
    in = new Pipe(pipeBufferSize);
    out = new Pipe(pipeBufferSize);
    oBigQ = new BigQ(*in, *out, oOrderMaker, runLen);
  }
  // Open file in read mode
  FILE *loadFile = fopen(loadpath, "r");
  try
  {
    if (loadFile == NULL)
    {
      throw("File:" + std::string(loadpath) + " error");
    }
    Record record;
    Page tempPage;
    int counter = 0;
    while (record.SuckNextRecord(&f_schema, loadFile) == 1)
    {
      counter++;
      if (counter % 10000 == 0)
      {
        cerr << "Record Counter: " << counter << "\n";
      }
      in->Insert(&record);
    }
    cout << "Records " + std::to_string(counter) + "added to file \n";
  }
  catch (exception e)
  {
    cerr << e.what();
    fclose(loadFile);
  }
}
/**
 * @brief Open the existing heap file.
 * @param f_path Path of file to open.
 * @return 1 on success and 0 on failure.
 */
int DBFileSorted::Open(const char *f_path)
{
  try
  {
    int temp;
    string metafileName;
    metafileName.append(f_path);
    metafileName.append(".meta");
    ifstream metafile;
    metafile.open(metafileName.c_str());
    if (!metafile)
      return 0;
    metafile >> temp;
    metafile >> runLen;
    metafile >> oOrderMaker;
    fType dbfileType = (fType)temp;
    metafile.close();
    oFile.Open(1, f_path);
    mode = reading;
    MoveFirst();
    // cout << "File: " << std::string(f_path) << " opened. \n";
  }
  catch (...)
  {
    cerr << "File " << std::string(f_path) << " open error. \n";
    return 0;
  }
  return 1;
}
/** 
 * @brief Move to the first record in file.
*/
void DBFileSorted::MoveFirst()
{
  if (mode == writing)
  {
    TwoWayMerge();
  }
  pageNumber = 0;
  if (oFile.GetLength() != 0)
  {
    oFile.GetPage(&oPage, pageNumber);
    // cout << "Moved to first Page \n";
  }
  else
  {
    cout << "No Pages available to MoveFirst \n";
  }
}
/**
 * @brief Close the open heap file.
 * @return 1 on success and 0 on failure.
 */
int DBFileSorted::Close()
{
  try
  {
    if (0 == oFile.GetLength() && mode == writing && oBigQ != NULL)
    {
      in->ShutDown();
      Record tempRecord;
      Page tempPage;
      while (out->Remove(&tempRecord) == 1)
      {
        int pageFull = tempPage.Append(&tempRecord);
        if (pageFull == 0)
        {
          if (oFile.GetLength() > 0)
          {
            oFile.AddPage(&tempPage, oFile.GetLength() - 1);
          }
          else
          {
            oFile.AddPage(&tempPage, 0);
          }
          tempPage.EmptyItOut();
          tempPage.Append(&tempRecord);
        }
      }
      if (oFile.GetLength() > 0)
      {
        oFile.AddPage(&tempPage, oFile.GetLength() - 1);
      }
      else
      {
        oFile.AddPage(&tempPage, 0);
      }
      tempPage.EmptyItOut();
      delete out;
      delete in;
      delete oBigQ;
      out = NULL;
      in = NULL;
      oBigQ = NULL;
    }
    else if (mode == writing)
    {
      TwoWayMerge();
    }
    int fileLength = oFile.Close();
    return 1;
  }
  catch (...)
  {
    cout << "Close error, file does not exist \n";
    return 0;
  }
}
/**
 * @brief Adds a new record to the end of file.
 * @param rec record that needs to be inserted.
 */
void DBFileSorted::Add(Record &rec)
{
  mode = writing;
  if (oBigQ == NULL)
  {
    in = new Pipe(pipeBufferSize);
    out = new Pipe(pipeBufferSize);
    oBigQ = new BigQ(*in, *out, oOrderMaker, runLen);
  }
  in->Insert(&rec);
}
/**
 * @brief Gets next record from the file and returns to user, where "next" is defined
 * relative to the current pointer. After return pointer in file is increment to next record.
 * @param fetchme Pointer to which record needs to returned.
 * @return 1 on success and 0 on failure.
 */
int DBFileSorted::GetNext(Record &fetchme)
{
  if (mode == writing)
  {
    TwoWayMerge();
  }
  if (oPage.GetFirst(&fetchme) == 0)
  {
    ++pageNumber;
    if (pageNumber >= oFile.GetLength() - 1)
    {
      return 0;
    }
    oFile.GetPage(&oPage, pageNumber);
    oPage.GetFirst(&fetchme);
  }
  return 1;
}
/**
 * @brief Gets next record from the file based on selection predicate (a conjuctive normal form expression),
 * where "next" is defined relative to the current pointer in selection predicate. After return pointer in file is increment to next record.
 * @param fetchme Pointer to which record needs to returned.
 * @param cnf Conjuctive normal form expression to filter the records.
 * @param literal 
 * @return 1 on success and 0 on failure.
 */
int DBFileSorted::GetNext(Record &fetchme, CNF &cnf, Record &literal)
{
  // ComparisonEngine comp;
  // while (GetNext(fetchme) == 1)
  // {
  //   if (comp.Compare(&fetchme, &literal, &cnf) == 1)
  //   {
  //     return 1;
  //   }
  // }
  // return 0;

  OrderMaker queryorder, cnforder;
  OrderMaker::QueryOrderMaker(oOrderMaker, cnf, queryorder, cnforder);
  ComparisonEngine cmp;
  int foundInSearch = BinarySearch(fetchme, queryorder, literal, cnforder, cmp);
  if (foundInSearch == 0){
    return 0;
  }else{
    do {
      if (cmp.Compare(&fetchme, &queryorder, &literal, &cnforder)){
          return 0;
      } 
      if (cmp.Compare(&fetchme, &literal, &cnf)){
         return 1;
      }
    } while(DBFileSorted::GetNext(fetchme));
    return 0;  
  }
}

int DBFileSorted::BinarySearch(Record &fetchme, OrderMaker &queryorder, Record &literal, OrderMaker &cnforder, ComparisonEngine &cmp) {
  if (!DBFileSorted::GetNext(fetchme)) 
    return 0;
  int result = cmp.Compare(&fetchme, &queryorder, &literal, &cnforder);
  if (result > 0){
    return 0;
  }else if (result == 0){
   return 1;
  }

  int low = pageNumber, high = oFile.GetLength()-2;
  while (high > low){
    int mid = (low+high)/2;
    oFile.GetPage(&oPage, mid);
    // page is empty condition check
    if(GetNext(fetchme) == 0){
      return 0;
    }
    result = cmp.Compare(&fetchme, &queryorder, &literal, &cnforder);
    switch (result)
    {
    case -1:
      low = mid;
      break;
    case 0:
      high = mid;
      break;
    case 1:
      high = mid - 1;
      break;
    default:
      break;
    }
  }

  oFile.GetPage(&oPage, low);
  do {  
    if (GetNext(fetchme) == 0){
      return 0;
    } 
    result = cmp.Compare(&fetchme, &queryorder, &literal, &cnforder);
  } while (result < 0);
  return result == 0;
}

void DBFileSorted::TwoWayMerge(void)
{
  mode = reading;
  doMerge();
  delete out;
  delete in;
  delete oBigQ;
  out = NULL;
  in = NULL;
  oBigQ = NULL;
  MoveFirst();
}

void DBFileSorted::doMerge()
{
  Record tempRecord;
  MoveFirst();
  if (oFile.GetLength() != 0)
  {
    while (GetNext(tempRecord) == 1)
    {
      in->Insert(&tempRecord);
    }
  }
  in->ShutDown();
  int fsize = oFile.Close();
  pageNumber = 0;
  oPage.EmptyItOut();
  while (out->Remove(&tempRecord) == 1)
  {
    int pageFull = oPage.Append(&tempRecord);
    if (pageFull == 0)
    {
      oFile.AddPage(&oPage, pageNumber++);
      oPage.EmptyItOut();
      oPage.Append(&tempRecord);
    }
  }
  oFile.AddPage(&oPage, pageNumber++);
  oPage.EmptyItOut();
}
