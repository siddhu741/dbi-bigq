#include "RelOp.h"
#include <gtest/gtest.h>
#include <iostream>
// #include "test.h"

extern struct AndList *final;
DBFile dbf_ps, dbf_p;

CNF cnf_ps, cnf_p;
Record lit_ps, lit_p;
SelectFile SF_ps, SF_p;
Schema *_schema = new Schema ("catalog", "partsupp");
Schema *_schema2 = new Schema ("catalog", "region");
extern "C" {
	int yyparse(void);   // defined in y.tab.c
	int yyfuncparse(void);   // defined in yyfunc.tab.c
	void init_lexical_parser (char *); // defined in lex.yy.c (from Lexer.l)
	void close_lexical_parser (); // defined in lex.yy.c
	void init_lexical_parser_func (char *); // defined in lex.yyfunc.c (from Lexerfunc.l)
	void close_lexical_parser_func (); // defined in lex.yyfunc.c
}

void get_cnf (char *input, Schema *left, CNF &cnf_pred, Record &literal) {
	init_lexical_parser (input);
  	if (yyparse() != 0) {
		cout << " Error: can't parse your CNF " << input << endl;
		exit (1);
	}
	cnf_pred.GrowFromParseTree (final, left, literal); // constructs CNF predicate
	cout << "130" << endl;
	close_lexical_parser ();
}

void init_SF_ps (char *pred_str, int numpgs) {
	dbf_ps.Open ("test/partsupp.bin");
	get_cnf (pred_str, _schema, cnf_ps, lit_ps);
	SF_ps.Use_n_Pages (numpgs);
}

void init_SF_ps2 (int numpgs) {
	dbf_ps.Open ("test/region.bin");
	//get_cnf (pred_str, _schema, cnf_ps, lit_ps);
	SF_ps.Use_n_Pages (numpgs);
}
void init_SF_p (char *pred_str, int numpgs) {
	dbf_p.Open ("test/region.bin");
	get_cnf (pred_str, _schema2, cnf_p, lit_p);
	SF_p.Use_n_Pages (numpgs);
}

int clear_pipe (Pipe &in_pipe, Schema *schema, bool print) {
	Record rec;
	int cnt = 0;
	while (in_pipe.Remove (&rec)) {
		if (print) {
			rec.Print (schema);
		}
		cnt++;
	}
	return cnt;
}

TEST(RelOp, SelectFile) {
	init_SF_ps2 (100);
	Pipe _ps (100);
	SF_ps.Run (dbf_ps, _ps, cnf_ps, lit_ps);
	SF_ps.WaitUntilDone ();

	int cnt = clear_pipe (_ps, _schema2, true);
	cout << "\n\n test 2 returned " << cnt << " records \n";

	dbf_ps.Close ();
    ASSERT_EQ(cnt, 5);
}

TEST(RelOp, SelectFile_CNF) {
    char *pred_ps = "(ps_supplycost > 999.98)";
	init_SF_ps (pred_ps, 100);
    Pipe _ps (100);
	SF_ps.Run (dbf_ps, _ps, cnf_ps, lit_ps);
	SF_ps.WaitUntilDone ();

	int cnt = clear_pipe (_ps, _schema, true);
	cout << "\n\n test 1 returned " << cnt << " records \n";

	dbf_ps.Close ();
    ASSERT_EQ(cnt, 1);
}

TEST(RelOp, Project)
{
    char *pred_p = "(r_regionkey < 5)";
	init_SF_p (pred_p, 100);

	Project P_p;
		Pipe _out (100);
		Pipe _p (100);
		int keepMe[] = {0,1,7};
		int numAttsIn = 9;
		int numAttsOut = 1;
	P_p.Use_n_Pages (100);

	SF_p.Run (dbf_p, _p, cnf_p, lit_p);
	P_p.Run (_p, _out, keepMe, numAttsIn, numAttsOut);

	SF_p.WaitUntilDone ();
	P_p.WaitUntilDone ();

	Attribute att3[] = {{"int", Int}};
	Schema out_sch ("out_sch", numAttsOut, att3);
	int cnt = clear_pipe (_out, &out_sch, true);

	cout << "\n\n test 3 returned " << cnt << " records \n";

	dbf_p.Close ();
    ASSERT_EQ(cnt, 5);
}

TEST(RelOp, SelectPipe)
{
    char *pred_p = "(r_regionkey < 5)";
	dbf_p.Open ("region.bin");
	get_cnf (pred_p, _schema2, cnf_p, lit_p);

	SelectPipe SP_r;

	SP_r.Use_n_Pages (100);
	SF_p.Use_n_Pages(100);

	
	Pipe _out (100);
	Pipe _p (100);

	SF_p.Run (dbf_p, _p, cnf_p, lit_p);
	SP_r.Run(_p, _out, cnf_p, lit_p);

	SF_p.WaitUntilDone ();
	SP_r.WaitUntilDone ();

	int cnt = clear_pipe (_out, _schema2, true);

	cout << "\n\n test 4 returned " << cnt << " records \n";

	dbf_p.Close ();
    ASSERT_EQ(cnt, 5);
}




int main(int argc, char **argv)
{
    testing::InitGoogleTest(&argc, argv);
    return RUN_ALL_TESTS();
}