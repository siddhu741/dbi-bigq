#ifndef REL_OP_H
#define REL_OP_H
#pragma ones

#include "Pipe.h"
#include "DBFile.h"
#include "Record.h"
#include "Function.h"
#include "BigQ.h"
#include <cstdlib>
#include <sstream>
#include <vector>
/**
 * @brief Implements set of Relational operator specifically: SelectPipe, SelectFile, Project,
 *  Join, DuplicateRemoval, Sum for Database. RelationalOp serves as base class for all the operators.
 */
class RelationalOp
{
private:
	int page;

public:
	virtual void WaitUntilDone(){};
	void Use_n_Pages(int n)
	{
		page = n;
	};
};
/**
 * @brief Loads records from DbFile to pipe at the same time filtering any records based on CNF.
 */
class SelectFile : public RelationalOp
{
	private:
		pthread_t thread;
		static void *selectFileWorker(void *arg);

	public:
		void Run(DBFile &inFile, Pipe &outPipe, CNF &selOp, Record &literal);
		void WaitUntilDone();
};
/**
 * @brief Filters out records from pipe based on provided CNF.
 */
class SelectPipe : public RelationalOp
{
	private:
		pthread_t thread;
		static void *selectPipeWorker(void *arg);

	public:
		void Run(Pipe &inPipe, Pipe &outPipe, CNF &selOp, Record &literal);
		void WaitUntilDone();
};
/**
 * @brief Select few attributes from each record based absed on the order and index number provided.
 */
class Project : public RelationalOp
{
	private:
		pthread_t thread;
		static void *projectWorker(void *arg);
	public:
		void Run(Pipe &inPipe, Pipe &outPipe, int *keepMe, int numAttsInput, int numAttsOutput);
		void WaitUntilDone();
};
/**
 * @brief Performs join of two input pipes based on CNF provided.
 */
class Join : public RelationalOp
{
	private:
		pthread_t thread;
		static void *joinWorker(void *arg);

	public:
		void Run(Pipe &inPipeL, Pipe &inPipeR, Pipe &outPipe, CNF &selOp, Record &literal);
		void WaitUntilDone();
};
/**
 * @brief Removes Duplicate records.
 */
class DuplicateRemoval : public RelationalOp
{
	private:
		pthread_t thread;
		static void *duplicateWorker(void *arg);

	public:
		void Run(Pipe &inPipe, Pipe &outPipe, Schema &mySchema);
		void WaitUntilDone();
};
/**
 * @brief Performs sum aggregate function of input pipes
 */
class Sum : public RelationalOp
{
	private:
		pthread_t thread;
		static void *sumWorker(void *arg);

	public:
		void Run(Pipe &inPipe, Pipe &outPipe, Function &computeMe);
		void WaitUntilDone();
};
/**
 * @brief Performs Grouping of data and computes sum for any measure in that group.
 */
class GroupBy : public RelationalOp
{
	private:
		pthread_t thread;
		static void *groupByWorker(void *arg);

	public:
		void Run(Pipe &inPipe, Pipe &outPipe, OrderMaker &groupAtts, Function &computeMe);
		void WaitUntilDone();
};
/**
 * @brief Outputs the records from a pipe to a FILE based on schema provided.
 */
class WriteOut : public RelationalOp
{
	private:
		pthread_t thread;
		static void *writeoutWorker(void *arg);

	public:
		void Run(Pipe &inPipe, FILE *outFile, Schema &mySchema);
		void WaitUntilDone() override;
};
#endif