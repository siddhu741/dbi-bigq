#ifndef STATISTICS_
#define STATISTICS_
#include "ParseTree.h"
#include <vector>
#include <string>
#include <fstream>
#include <iostream>
#include <sstream>
#include <unordered_map>
#include <unordered_set>
#include <unordered_map>
using namespace std;
struct RelationStruct
{

	unordered_map<string, double> mapAttirbute;
	int numAttr;
	RelationStruct(){};
	RelationStruct(int numAttr) : numAttr(numAttr) {}
	void AddRelation(char *attrName, int numDistincts)
	{
		mapAttirbute[string(attrName)] = numDistincts;
	}
	void operator=(const RelationStruct &self)
	{
		this->numAttr = self.numAttr;
		this->mapAttirbute = self.mapAttirbute;
	}
	void AddAttribute(string Name, int val)
	{
		mapAttirbute[Name] = val;
	}
	int getNumAttr()
	{
		return numAttr;
	}
	unordered_map<string, double> getMapAttribute()
	{
		return mapAttirbute;
	}
	friend std::ostream &operator<<(std::ostream &os, const RelationStruct &relation);
	friend std::istream &operator>>(std::istream &is, RelationStruct &oRelationStruct);
};

class Statistics
{
	unordered_map<string, RelationStruct> mapRelation;
	unordered_map<string, double> mapEstimate;
	bool validateRelation(char *relName[], int numJoin);
	bool validateAttributes(struct Operand *left, double &t, char **relNames, int numToJoin);
	// split method overloading
	std::unordered_set<string> split(string input)
	{
		string line;
		unordered_set<string> outSetString;
		stringstream oStringStream(input);
		while (getline(oStringStream, line, '#'))
		{
			outSetString.insert(line);
		}
		return outSetString;
	}

public:
	Statistics();
	Statistics(Statistics &copyMe); // Performs deep copy
	~Statistics();

	void AddRel(char *relName, int numTuples);
	void AddAtt(char *relName, char *attName, int numDistincts);
	void CopyRel(char *oldName, char *newName);

	void Read(char *fromWhere);
	void Write(char *fromWhere);

	void Apply(struct AndList *parseTree, char *relNames[], int numToJoin);
	double Estimate(struct AndList *parseTree, char **relNames, int numToJoin);
	// operator overloading for read write
	friend std::ostream &operator<<(std::ostream &os, const Statistics &stat);
	friend std::istream &operator>>(std::istream &is, Statistics &stat);

	int ParseRelation(string name, string &rel);
	void LoadAllStatistics();
};

#endif