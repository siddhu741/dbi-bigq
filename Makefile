
CC = g++ -O2 -Wno-deprecated

tag = -i
test_out_tag = -ll

ifdef linux
tag = -n
test_out_tag = -lfl
endif

a5.out: Record.o Comparison.o ComparisonEngine.o Schema.o File.o DBFile.o DBFileHeap.o DBFileSorted.o DBFileInterface.o Pipe.o BigQ.o RelOp.o Function.o Statistics.o QueryNode.o Optimizer.o y.tab.o yyfunc.tab.o lex.yy.o lex.yyfunc.o main.o
	$(CC) -o a5.out Record.o Comparison.o ComparisonEngine.o Schema.o File.o DBFile.o DBFileHeap.o DBFileSorted.o DBFileInterface.o Pipe.o BigQ.o RelOp.o Function.o Statistics.o QueryNode.o Optimizer.o y.tab.o yyfunc.tab.o lex.yy.o lex.yyfunc.o main.o -lfl -lpthread

main.out: Record.o Comparison.o ComparisonEngine.o Schema.o File.o DBFile.o DBFileHeap.o DBFileSorted.o DBFileInterface.o Pipe.o BigQ.o RelOp.o Function.o Statistics.o QueryNode.o Optimizer.o y.tab.o yyfunc.tab.o lex.yy.o lex.yyfunc.o main.o
	$(CC) -o main.out Record.o Comparison.o ComparisonEngine.o Schema.o File.o DBFile.o DBFileHeap.o DBFileSorted.o DBFileInterface.o Pipe.o BigQ.o RelOp.o Function.o Statistics.o QueryNode.o Optimizer.o y.tab.o yyfunc.tab.o lex.yy.o lex.yyfunc.o main.o -lfl -lpthread

a42.out: Record.o Comparison.o ComparisonEngine.o Schema.o File.o DBFile.o DBFileHeap.o DBFileSorted.o DBFileInterface.o Pipe.o BigQ.o RelOp.o Function.o Statistics.o QueryNode.o Optimizer.o y.tab.o yyfunc.tab.o lex.yy.o lex.yyfunc.o main.o
	$(CC) -o a42.out Record.o Comparison.o ComparisonEngine.o Schema.o File.o DBFile.o DBFileHeap.o DBFileSorted.o DBFileInterface.o Pipe.o BigQ.o RelOp.o Function.o Statistics.o QueryNode.o Optimizer.o y.tab.o yyfunc.tab.o lex.yy.o lex.yyfunc.o main.o -lfl -lpthread

a4-1.out: Record.o Comparison.o ComparisonEngine.o Schema.o File.o DBFile.o DBFileHeap.o DBFileSorted.o DBFileInterface.o Pipe.o BigQ.o RelOp.o Function.o Statistics.o y.tab.o lex.yy.o test.o
	$(CC) -o a4-1.out Record.o Comparison.o ComparisonEngine.o Schema.o File.o DBFile.o DBFileHeap.o DBFileSorted.o DBFileInterface.o Pipe.o BigQ.o RelOp.o Function.o Statistics.o y.tab.o lex.yy.o test.o -lfl -lpthread
	
a3test.out: Record.o Comparison.o ComparisonEngine.o Schema.o File.o DBFile.o DBFileHeap.o DBFileSorted.o DBFileInterface.o Pipe.o BigQ.o RelOp.o Function.o y.tab.o yyfunc.tab.o lex.yy.o lex.yyfunc.o a3test.o
	$(CC) -o a3test.out Record.o Comparison.o ComparisonEngine.o Schema.o File.o DBFile.o DBFileHeap.o DBFileSorted.o DBFileInterface.o Pipe.o BigQ.o RelOp.o Function.o y.tab.o yyfunc.tab.o lex.yy.o lex.yyfunc.o a3test.o -lfl -lpthread

a2test.out: Record.o Comparison.o ComparisonEngine.o Schema.o File.o BigQ.o DBFile.o DBFileHeap.o DBFileSorted.o DBFileInterface.o Pipe.o y.tab.o lex.yy.o a2test.o
	$(CC) -o a2test.out Record.o Comparison.o ComparisonEngine.o Schema.o File.o BigQ.o DBFile.o DBFileHeap.o DBFileSorted.o DBFileInterface.o Pipe.o y.tab.o lex.yy.o a2test.o $(test_out_tag) -lpthread
	

a1test.out: Record.o Comparison.o ComparisonEngine.o Schema.o File.o BigQ.o DBFile.o DBFileHeap.o DBFileSorted.o DBFileInterface.o Pipe.o y.tab.o lex.yy.o a1test.o
	$(CC) -o a1test.out Record.o Comparison.o ComparisonEngine.o Schema.o File.o BigQ.o DBFile.o DBFileHeap.o DBFileSorted.o DBFileInterface.o Pipe.o y.tab.o lex.yy.o a1test.o $(test_out_tag) -lpthread

a2test2.out: Record.o Comparison.o ComparisonEngine.o Schema.o File.o BigQ.o DBFile.o DBFileHeap.o DBFileSorted.o DBFileInterface.o Pipe.o y.tab.o lex.yy.o a2test2.o
	$(CC) -o a2test2.out Record.o Comparison.o ComparisonEngine.o Schema.o File.o BigQ.o DBFile.o DBFileHeap.o DBFileSorted.o DBFileInterface.o Pipe.o y.tab.o lex.yy.o a2test2.o $(test_out_tag) -lpthread

executeTests.out: Record.o Comparison.o ComparisonEngine.o Schema.o File.o y.tab.o lex.yy.o DBFile.o DBFileHeap.o DBFileInterface.o DBFileTest.o
	$(CC) -o executeTests.out Record.o Comparison.o ComparisonEngine.o Schema.o File.o y.tab.o lex.yy.o DBFile.o DBFileHeap.o DBFileInterface.o DBFileTest.o $(test_out_tag) -lpthread -lgtest

runStatisticsTests.out: Record.o Comparison.o ComparisonEngine.o Schema.o File.o DBFile.o DBFileHeap.o DBFileSorted.o DBFileInterface.o Pipe.o BigQ.o RelOp.o Function.o y.tab.o yyfunc.tab.o lex.yy.o lex.yyfunc.o Statistics.o StatisticsTest.o
	$(CC) -o runStatisticsTests.out Record.o Comparison.o ComparisonEngine.o Schema.o File.o DBFile.o DBFileHeap.o DBFileSorted.o DBFileInterface.o Pipe.o BigQ.o RelOp.o Function.o y.tab.o yyfunc.tab.o lex.yy.o lex.yyfunc.o Statistics.o StatisticsTest.o $(test_out_tag) -lpthread -lgtest

runOptimizerTests.out: Record.o Comparison.o ComparisonEngine.o Schema.o File.o DBFile.o DBFileHeap.o DBFileSorted.o DBFileInterface.o Pipe.o BigQ.o RelOp.o Function.o Statistics.o QueryNode.o Optimizer.o y.tab.o yyfunc.tab.o lex.yy.o lex.yyfunc.o OptimizerTest.o
	$(CC) -o runOptimizerTests.out Record.o Comparison.o ComparisonEngine.o Schema.o File.o DBFile.o DBFileHeap.o DBFileSorted.o DBFileInterface.o Pipe.o BigQ.o RelOp.o Function.o Statistics.o QueryNode.o Optimizer.o y.tab.o yyfunc.tab.o lex.yy.o lex.yyfunc.o OptimizerTest.o $(test_out_tag) -lpthread -lgtest

runFinalTests.out: Record.o Comparison.o ComparisonEngine.o Schema.o File.o DBFile.o DBFileHeap.o DBFileSorted.o DBFileInterface.o Pipe.o BigQ.o RelOp.o Function.o Statistics.o QueryNode.o Optimizer.o y.tab.o yyfunc.tab.o lex.yy.o lex.yyfunc.o FinalTest.o
	$(CC) -o runFinalTests.out Record.o Comparison.o ComparisonEngine.o Schema.o File.o DBFile.o DBFileHeap.o DBFileSorted.o DBFileInterface.o Pipe.o BigQ.o RelOp.o Function.o Statistics.o QueryNode.o Optimizer.o y.tab.o yyfunc.tab.o lex.yy.o lex.yyfunc.o FinalTest.o $(test_out_tag) -lpthread -lgtest

main.o : main.cc
	$(CC) -g -c main.cc

Optimizer.o: Optimizer.cc
	$(CC) -g -c Optimizer.cc

OptimizerTest.o: OptimizerTest.cc
	$(CC) -g -c OptimizerTest.cc

FinalTest.o: FinalTest.cc
	$(CC) -g -c FinalTest.cc

test42.o: test42.cc
	$(CC) -g -c test42.cc

QueryNode.o: QueryNode.cc
	$(CC) -g -c QueryNode.cc

MetaStruct.o: MetaStruct.cc
	$(CC) -g -c MetaStruct.cc

Statistics.o: Statistics.cc
	$(CC) -g -c Statistics.cc

a3test.o: a3test.cc
	$(CC) -g -c a3test.cc

a1test.o: a1test.cc
	$(CC) -g -c a1test.cc

a2test.o: a2test.cc
	$(CC) -g -c a2test.cc

a2test2.o: a2test2.cc
	$(CC) -g -c a2test2.cc

test.o: test.cc
	$(CC) -g -c test.cc

Comparison.o: Comparison.cc
	$(CC) -g -c Comparison.cc
	
ComparisonEngine.o: ComparisonEngine.cc
	$(CC) -g -c ComparisonEngine.cc
	
Pipe.o: Pipe.cc
	$(CC) -g -c Pipe.cc

BigQ.o: BigQ.cc
	$(CC) -g -c BigQ.cc

RelOp.o: RelOp.cc
	$(CC) -g -c RelOp.cc

DBFile.o: DBFile.cc
	$(CC) -g -c DBFile.cc

DBFileInterface.o: DBFileInterface.cc
	$(CC) -g -c DBFileInterface.cc

DBFileHeap.o: DBFileHeap.cc
	$(CC) -g -c DBFileHeap.cc

DBFileSorted.o: DBFileSorted.cc
	$(CC) -g -c DBFileSorted.cc

File.o: File.cc
	$(CC) -g -c File.cc

Record.o: Record.cc
	$(CC) -g -c Record.cc

Schema.o: Schema.cc
	$(CC) -g -c Schema.cc
	
y.tab.o: Parser.y
	yacc -d Parser.y
	sed $(tag) y.tab.c -e "s/  __attribute__ ((__unused__))$$/# ifndef __cplusplus\n  __attribute__ ((__unused__));\n# endif/" 
	g++ -c y.tab.c

yyfunc.tab.o: ParserFunc.y
	yacc -p "yyfunc" -b "yyfunc" -d ParserFunc.y
	#sed $(tag) yyfunc.tab.c -e "s/  __attribute__ ((__unused__))$$/# ifndef __cplusplus\n  __attribute__ ((__unused__));\n# endif/" 
	g++ -c yyfunc.tab.c

lex.yy.o: Lexer.l
	lex  Lexer.l
	gcc  -c lex.yy.c

lex.yyfunc.o: LexerFunc.l
	lex -Pyyfunc LexerFunc.l
	gcc  -c lex.yyfunc.c

clean: 
	rm -f *.o
	rm -f *.out
	rm -f y.tab.*
	rm -f lex.yy.*
	rm -f yyfunc.tab.*
	rm -f lex.yyfunc*

all: a2-test.out a1-test.out a22.out a3.out a4-1.out main.out