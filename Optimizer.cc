#include "Optimizer.h"
using namespace std;

/**
 * @brief Helper method to print the tree 
 * 
 */
void Optimizer::PrintQuery(){
    if(nodeRoot != nullptr){
        nodeRoot->Print();
    }else{
		cout << "The Tree is null" << endl;
        return ;
	}
}

/**
 * @brief 
 * if join contains more than 1, the methid uses "Estimate" method of Statistics class to choose minimum cost element, and recursively reduces the size of joins array.
 * if size of joins == 1 or less than 1, the method returns the joins as it is.
 * 
 * @param joins list of AndList which are to be processed
 * @return vector<AndList*> processed joinsList
 */
vector<AndList*> Optimizer::processJoins(vector<AndList*> joins) {
	vector<AndList*> returnList;
	if(joins.size() > 1){
		returnList.reserve(joins.size());
		int size = joins.size();
		int i = 0;
		while (i < size){
			double minCostToChoose = 0.0;
			string relation_left = "", relation_right = "";
			AndList *selected_min_chooselist = NULL;
			int selected_position = -1;
			int j = 0;
			while (j < size){
				AndList *curList = joins[j];
				string relation_first, relation_second;

				Operand *r = curList->left->left->right;
				Operand *l = curList->left->left->left;
				this->s->ParseRelation(string(l->value), relation_first);
				this->s->ParseRelation(string(r->value), relation_second);

				char *relation_array_strings[] = {(char*)relation_first.c_str(), (char*)relation_second.c_str()};
				double curEstimatedCost = this->s->Estimate(curList,relation_array_strings, 2);
				if(selected_position == -1 || curEstimatedCost < minCostToChoose) {
					selected_min_chooselist = curList;
					selected_position = j;
					minCostToChoose = curEstimatedCost;
					relation_left = relation_first;
					relation_right = relation_second;
				}
				j ++;
			}
			returnList.emplace_back(selected_min_chooselist);
			char *aplyrels[] = {(char*) relation_left.c_str(), (char*)relation_right.c_str()};
			this->s->Apply(selected_min_chooselist, aplyrels, 2);
			joins.erase(joins.begin()+selected_position);
			i ++;
		}
		return returnList;
	}
	return joins;
	
	
}

/**
 * @brief This method returns a map of relation and lists from input.
 * 
 * @param allAndLists 
 * @return unordered_map<string, AndList *> 
 */
unordered_map<string, AndList *> Optimizer::getSelectors(vector<AndList *> allAndLists){
    unordered_map<string,AndList *> returnMap;
	for(int i=0; i < allAndLists.size(); i++){
		string rel;
		AndList *eachList = allAndLists[i];
		Operand *operand = eachList->left->left->left;
		if(operand->code != NAME){
			operand = eachList->left->left->right;
		}
		
		s->ParseRelation(string(operand->value), rel);
		bool broken = false;
        for(auto mapIterator=returnMap.begin(); mapIterator!=returnMap.end(); mapIterator++){
            if(mapIterator->first.compare(rel) == 0) {
                AndList *lastAnd = mapIterator->second;
                while(lastAnd->rightAnd != NULL){
                    lastAnd = lastAnd->rightAnd;
				}
                lastAnd->rightAnd = eachList;
				broken = true;
                break;
            }
        }
        if(!broken){
            returnMap[rel] = eachList;
		}
    }
    return returnMap;
}

/**
 * @brief : This method takes in a schema and generates an instance of OrderMaker class and return the orderMaker
 * 
 * @param schema : This is instance of Schema class for which we want to generate an Order Maker instance
 * @return OrderMaker* 
 */
OrderMaker *Optimizer::getOrderMakerFromSchema(Schema *schema) {
	NameList *name = this->groupAttributes;
	OrderMaker *outputOrder = new OrderMaker();
	while (name){
		outputOrder->whichAtts[outputOrder->numAtts] = schema->Find(name->name);
		outputOrder->whichTypes[outputOrder->numAtts] = schema->FindType(name->name);
		outputOrder->numAtts = outputOrder->numAtts + 1;
		name = name->next;
	}
	return outputOrder;
}

/**
 * @brief Helper method to node's execute methid
 * 
 */
void Optimizer::ExecuteQuery(){
    mtx.lock();
	if(nodeRoot==nullptr){
		std::cout<<"The tree is null\n";
		return ;
	}
	//pthread_t thread1;
	//pthread_create (&thread1, NULL, Ex, (void *)&root);
	//clear_pipe(*(root->outPipe),root->outputSchema,true);
	//root->wait();
	WriteOutNode *wr=new WriteOutNode();
	wr->left = nodeRoot;
	wr->lPipe = wr->left->oPipe;
	wr->outputSchema = wr->left->outputSchema;
	//wr->Print();
	wr->Execute();
	wr->wait();
	delete wr;
	mtx.unlock();
}

/**
 * @brief This method processes the instance variable "cnfAndList" of Optimizer class and seperates them into three parts joins, selects, selAboveJoin
 * 
 * @param joins 
 * @param selects 
 * @param selAboveJoin 
 */
void Optimizer::JoinsAndSelects(vector<AndList*> &joins, vector<AndList*> &selects, vector<AndList*> &selAboveJoin){
    OrList *curOrList;
    AndList *cnfAndListPtr = this->cnfAndList;
    while(cnfAndListPtr){
		if (cnfAndListPtr->left){
			curOrList = cnfAndListPtr->left;
			if(curOrList->left->code == EQUALS && curOrList->left->left->code == NAME && curOrList->left->right->code == NAME){ 
				AndList *tempAndList = new AndList();
				tempAndList->left= curOrList;
				tempAndList->rightAnd = NULL;
				joins.push_back(tempAndList);
			} else if(!curOrList->rightOr) {
				AndList *newAnd = new AndList();
				newAnd->left= curOrList;
				newAnd->rightAnd = NULL;
				selects.push_back(newAnd);
			} else{
				vector<string> relationsNamesList;
				OrList *olp = curOrList;
				while(curOrList != NULL){
					string rel;
					Operand *op = curOrList->left->left;
					if(op->code != NAME){
						op = curOrList->left->right;
					}
					
					if(s->ParseRelation(string(op->value), rel) == 0) {
						cerr <<"Error in parse relations"<<endl;
						return;
					}
					if(relationsNamesList.size() == 0){
						relationsNamesList.push_back(rel);
					}else if(rel.compare(relationsNamesList[0]) != 0){
						relationsNamesList.push_back(rel);
					}
					curOrList = curOrList->rightOr;
				}
				AndList *temp = new AndList();
				temp->left= olp;
				temp->rightAnd = nullptr;
				if(relationsNamesList.size() > 1){
					selAboveJoin.push_back(temp);
				}else{
					selects.push_back(temp);
				}
			}
			cnfAndListPtr = cnfAndListPtr->rightAnd;
		}else{
			cerr << "Error in cnf AndList" << endl;
			return;
		}
        
	}
}

/**
 * @brief Main Construction method to instantiate the Optimizer class. The main.cc calls this method to instantiate the Optimizer object. 
 * This method creates nodes based on the initial CNF and builds a tree. This methid uses the above "JoinsAndSelects" method initially to split "cnfAndList".
 * It then creates nodes based on instance variables of Optimizer class
 * 
 * @param finalFunction 
 * @param tables 
 * @param boolean 
 * @param pGrpAtts 
 * @param pAttsToSelect 
 * @param distinct_atts 
 * @param distinct_func 
 * @param s 
 * @param dir 
 * @param tpch 
 * @param catalog 
 * @return Optimizer:: 
 */
Optimizer:: Optimizer(struct FuncOperator *finalFunction,
	struct TableList *tables, struct AndList * boolean, struct NameList * pGrpAtts, struct NameList * pAttsToSelect,
	int distinct_atts, int distinct_func, Statistics *s,string dir,string tpch,string catalog):nodeRoot(new Node()),
	finalFunction(finalFunction), tables(tables),
	cnfAndList(boolean),
	groupAttributes(pGrpAtts),
	attributesSelected(pAttsToSelect),
	booleanDistinctAttribute(booleanDistinctAttribute),
	booleanDistinctFunction(distinct_func),
	s(s){

	TableList *curTableList = tables;
	vector<AndList*> joins, selectors, selAboveJoin;
	JoinsAndSelects(joins,selectors,selAboveJoin);
	unordered_map<string,AndList *> select(move(getSelectors(selectors)));
	while(curTableList){
		if(curTableList->aliasAs){
			s->CopyRel(curTableList->tableName,curTableList->aliasAs);
		}
		curTableList = curTableList->next;
	}
	nodeRoot = new Node();
	vector<AndList *> orderJoin(move(processJoins(joins)));
	
	//Select Node
	map<string,Node *> relationToSelectNodeMap;
	curTableList = tables;
	while(curTableList){
		Node *selectFileNode = new SelectFNode();
		selectFileNode->dbfilePath = dir + string(curTableList->tableName);
		selectFileNode->oPipe = pipeSelect;
		pipeSelect = pipeSelect + 1;

		selectFileNode->outputSchema = new Schema(&catalog[0u], curTableList->tableName);
		string relName(curTableList->tableName);
		if(curTableList->aliasAs){
			selectFileNode->outputSchema->AdjustSchemaWithAlias(curTableList->aliasAs);
			relName = string(curTableList->aliasAs);
		}
		AndList *andList = nullptr;
		for(auto it: select){
			if(!relName.compare(it.first)){
				andList=it.second;
				break;
			}
		}
		selectFileNode->cnf = new CNF();
		selectFileNode->cnf->GrowFromParseTree(andList,selectFileNode->outputSchema,*(selectFileNode->literal));
		relationToSelectNodeMap.emplace(relName,selectFileNode);
		curTableList = curTableList->next;
	}
	
	//---------------------------------------------------------------

	//Join Node
	Node *joinNode = nullptr;
	unordered_map<string,Node *> leftRightJoinMap;
	for(auto it: orderJoin){
		joinNode = new JoinNode();
		AndList *inner = it;
		Operand *left = inner->left->left->left;
		Operand *right = inner->left->left->right;
		string relationLeft = left->value;
		string relationRight=right->value;
		s->ParseRelation(string(left->value), relationLeft);
		s->ParseRelation(string(right->value),relationRight);
		Node *leftUpMost = nullptr; 
		Node *rightUpMost = nullptr;
		if(leftRightJoinMap.count(relationLeft)) {
			leftUpMost = leftRightJoinMap[relationLeft];
		}
		if(leftRightJoinMap.count(relationRight)) {
			rightUpMost=leftRightJoinMap[relationRight];
		}

		if(!leftUpMost && !rightUpMost){
			joinNode->left = relationToSelectNodeMap[relationLeft];
			joinNode->right = relationToSelectNodeMap[relationRight];
			joinNode->outputSchema = new Schema(joinNode->left->outputSchema,joinNode->right->outputSchema);
		}else if(leftUpMost){
			while(leftUpMost->parent){
				leftUpMost = leftUpMost->parent;
			}
			joinNode->left = leftUpMost; 
			leftUpMost->parent = joinNode;
			joinNode->right = relationToSelectNodeMap[relationRight];
		}else if(rightUpMost) { //!A and B
			while(rightUpMost->parent){
				rightUpMost = rightUpMost->parent;
			}
			joinNode->left = rightUpMost;
			rightUpMost->parent = joinNode;
			joinNode->right = relationToSelectNodeMap[relationLeft];
		}else { // A and B
			while(leftUpMost->parent){
				leftUpMost = leftUpMost->parent;
			}
			while(rightUpMost->parent){
				rightUpMost = rightUpMost->parent;
			}
			joinNode->left = leftUpMost;
			leftUpMost->parent = joinNode;
			joinNode->right = rightUpMost;
			rightUpMost->parent = joinNode;
		}
		leftRightJoinMap[relationLeft] = joinNode;
		leftRightJoinMap[relationRight] = joinNode;
		joinNode->lPipe = joinNode->left->oPipe;
		joinNode->rPipe = joinNode->right->oPipe;
		joinNode->outputSchema = new Schema(joinNode->left->outputSchema, joinNode->right->outputSchema);
		joinNode->oPipe = pipeSelect++;	
		joinNode->cnf = new CNF();
		joinNode->cnf->GrowFromParseTree(inner, joinNode->left->outputSchema, joinNode->right->outputSchema, *(joinNode->literal));
	}
	//-----------------------------------------------------------

	//Select Above join
	Node *selectAboveJoinNode = nullptr;
	if(selAboveJoin.size() > 0) {
		selectAboveJoinNode = new SelectPNode();
		if(joinNode != NULL){
			selectAboveJoinNode->left = joinNode;
		}else{
			selectAboveJoinNode->left = relationToSelectNodeMap.begin()->second;
		}
		selectAboveJoinNode->lPipe = selectAboveJoinNode->left->oPipe;
		selectAboveJoinNode->oPipe = pipeSelect;
		pipeSelect = pipeSelect + 1;
		selectAboveJoinNode->outputSchema = selectAboveJoinNode->left->outputSchema;
		AndList *andList = *(selAboveJoin.begin());
		for(auto it=selAboveJoin.begin(); it!= selAboveJoin.end(); it++){
			if(it != selAboveJoin.begin()){
				andList->rightAnd = *it;
			}
		}
		selectAboveJoinNode->cnf->GrowFromParseTree(andList, selectAboveJoinNode->outputSchema, *(selectAboveJoinNode->literal));
	}


	//Building GroupBy
	Node *groupByNode=nullptr;
	if(groupAttributes){
		groupByNode = new GroupByNode();
		Attribute DA = {"double", Double};
		Attribute attr;
		if(selectAboveJoinNode) {
			groupByNode ->left = selectAboveJoinNode;
		}else if(joinNode) {
			groupByNode->left = joinNode;
		}else {
			groupByNode->left = relationToSelectNodeMap.begin()->second;
		}
		groupByNode->lPipe = groupByNode->left->oPipe;
		groupByNode->oPipe = pipeSelect;
		pipeSelect = pipeSelect + 1;
		groupByNode->function = getFunctionFromSchema(groupByNode->left->outputSchema);
		groupByNode->order = getOrderMakerFromSchema(groupByNode->left->outputSchema);
		
		attr.name = (char *)"sum";
		attr.myType = Double;
		NameList *attName = groupAttributes;
		Schema *schema = new Schema ((char *)"dummy", 1, &attr);
		int numGroupAttr = 0;
		while(attName){
			numGroupAttr++;
			attName = attName->next;
		}
		if(!numGroupAttr){
			groupByNode->outputSchema=schema;
		}else{
			Attribute *attrs = new Attribute[numGroupAttr];
			int i = 0;
			attName = groupAttributes;
			while(attName) {
				attrs[i].name = &string(attName->name)[0u];
				attrs[i++].myType = groupByNode->left->outputSchema->FindType(attName->name);
				attName = attName->next;
			}
			Schema *outSchema = new Schema((char *)"dummy", numGroupAttr, attrs);
			groupByNode->outputSchema = new Schema(schema, outSchema);
		}
	}

	//Sum Node
	Node *sumNode = nullptr;
	if(groupByNode == NULL && this->finalFunction != NULL){
		sumNode = new SumNode();
		Attribute attr;
		if(selectAboveJoinNode){
			sumNode->left = selectAboveJoinNode;
		}else if(joinNode){
			sumNode->left = joinNode;
		}else{
			sumNode->left = relationToSelectNodeMap.begin()->second;
		}
		sumNode->lPipe = sumNode->left->oPipe;
		sumNode->oPipe = pipeSelect++;
		sumNode->function = this->getFunctionFromSchema(sumNode->left->outputSchema);
		attr.name = (char*)"sum";
		attr.myType = Double; 
		sumNode->outputSchema = new Schema((char *)"Dummy", 1, &attr);
	}
	//---------------------------------------------------------------------

	//Project 
	Node *projectNode = new ProjectNode();
	Attribute *outputAtts;
	NameList *name = attributesSelected;
	int ithAttr = 0, countAttr = 0;
	while(name) {  
		countAttr++;
		name = name->next;
	}
	if(sumNode) { // we have SUM
		projectNode->left = sumNode;
		countAttr++;
		projectNode->keepMe = new int[countAttr];
		projectNode->keepMe[0] = sumNode->outputSchema->Find((char *) "sum");
		outputAtts = new Attribute[countAttr];
		outputAtts[0].name = (char*)"sum";
		outputAtts[0].myType = Double;
		ithAttr = 1;
	}else if(groupByNode){
		projectNode->left = groupByNode;
		countAttr++;
		projectNode->keepMe = new int[countAttr];
		projectNode->keepMe[0] = groupByNode->outputSchema->Find((char *)"sum");
		outputAtts = new Attribute[countAttr+1];
		outputAtts[0].name = (char *)"sum";
		outputAtts[0].myType = Double;
		ithAttr = 1;
	}else if(joinNode) {
		projectNode->left = joinNode;
		if(countAttr =! 0) {
			projectNode->keepMe = new int[countAttr];
			outputAtts = new Attribute[countAttr];
		}else{
			cerr << "error as no attributes are specified!" << endl;
			return;
		}
	}else{
		projectNode->left = relationToSelectNodeMap.begin()->second;
		if(countAttr != 0) {
			projectNode->keepMe = new int[countAttr];
			outputAtts = new Attribute[countAttr];
		}else{
			cerr <<"No attributes assigned to select!"<<endl;
			return ;
		}
	}
	name = this->attributesSelected;
	while(name) {
		projectNode->keepMe[ithAttr] = projectNode->left->outputSchema->Find(name->name);
		outputAtts[ithAttr].name = name->name;
		outputAtts[ithAttr].myType = projectNode->left->outputSchema->FindType(name->name);
		ithAttr++;
		name = name->next;
	}
	projectNode->numAttsInput = projectNode->left->outputSchema->GetNumAtts();
	projectNode->numAttsOutput = countAttr;
	projectNode->lPipe = projectNode->left->oPipe;
	projectNode->oPipe = pipeSelect++;
	projectNode->outputSchema = new Schema((char*)"dummy", countAttr, outputAtts);
	nodeRoot = projectNode;
	//------------------------------------------------------------
	
	//Distinct
	Node *distinctNode = nullptr;
	if(booleanDistinctAttribute){
		distinctNode = new DistinctNode();
		distinctNode->left = projectNode;
		distinctNode->lPipe = distinctNode->left->oPipe;
		distinctNode->outputSchema = distinctNode->left->outputSchema;
		distinctNode->oPipe = pipeSelect;
		pipeSelect = pipeSelect + 1;
		nodeRoot = distinctNode;
	}
	//-----------------------------------------------------------
}

/**
 * @brief This method generates a Function from the given object instance of Schema 
 * 
 * @param schema 
 * @return Function* 
 */
Function *Optimizer::getFunctionFromSchema(Schema *schema) {
	Function *returnFunction = new Function();
	returnFunction->GrowFromParseTree(finalFunction, *schema);
	return returnFunction;
}


/**
 * @brief This method helps in supporing Create query execution
 * 
 * @param catalog_path Path to the catalog file containing schema information.
 * @param dir Directory where to create table
 * @param create CreateTable Structure
 * @return bool Boolean value specifying if query was succesfull or not. 
 */
bool Optimizer::CreateQuery(string catalog_path,string dir,CreateTable *create){
	mtx.lock();
	DBFile *db = new DBFile;
	string temp=dir+string(create->tableName)+".bin";
	OrderMaker *om = new OrderMaker;
	if(create->type == SORTED) {
		NameList *sortAtt = create->sortAttrList;
		while(sortAtt) {
			AttrList *atts = create->attrList;
			int i=0;
			while(atts) {
				if(strcmp(sortAtt->name, atts->attr->attrName)){
					om->whichAtts[om->numAtts] = i;
					om->whichTypes[om->numAtts] = (Type) atts->attr->type;
					om->numAtts++;
					break;
				}
				i++;
				atts = atts->next;
			}
			sortAtt = sortAtt->next;
		}
		struct test { OrderMaker *o; 
		int l;};
		test * pOrder=new test();
		pOrder->o=om;
		pOrder->l=5;
		db->Create(&temp[0u], sorted, (void*)pOrder);
	} else
		db->Create(&temp[0u], heap, NULL );
	db->Close();
	delete db;
	mtx.unlock();
	return 1;
}

/**
 * @brief This method helps in supporing Insert query execution
 * 
 * @param catalog_path Path to the catalog file containing schema information.
 * @param dir Directory where table .bin file is located
 * @param tpch_dir Directory location where .tbl files are located.
 * @param insert CreateTable Structure
 * @return bool Boolean value specifying if query was succesfull or not. 
 */
bool Optimizer::InsertQuery(string catalog_path,string dir,string tpch_dir,InsertFile *insert){
	mtx.lock();
	DBFile *dbfile=new DBFile();
	string dbpath=dir+insert->tableName+".bin";
	dbfile->Open(&dbpath[0u]);
	string fpath=tpch_dir+insert->fileName+".tbl";
	cout <<"loading " <<fpath<<endl;
	Schema schema(&catalog_path[0u], insert->tableName);
	dbfile->Load(schema, &fpath[0u]);
	dbfile->Close();
	delete dbfile;
	mtx.unlock();
	return 1;
}
/**
 * @brief This method helps in supporing Drop query execution
 * 
 * @param catalog_path Path to the catalog file containing schema information.
 * @param dir Directory where table .bin file is located
 * @param name Name of the table to delete.
 * @return bool Boolean value specifying if query was succesfull or not. 
 */
bool Optimizer::DropTable(string catalog,string dir,string name){
	string temp=dir+name+".bin";
	if(remove(&temp[0u]))
		return false;
	temp=dir+name+".bin.meta";
	if(remove(&temp[0u]))
		return false;
	return true;
}

int clear_pipe (Pipe &in_pipe, Schema *schema, bool print) {
	Record rec;
	int cnt = 0;
	while (in_pipe.Remove (&rec)) {
		if (print) {
			rec.Print (schema);
		}
		cnt++;
	}
	return cnt;
}