#include "BigQ.h"
#include<stdlib.h>
#include<vector>
#include<cmath>
#include<algorithm>
#include<queue>
#include<string>
#include<sstream>
using namespace std;

//global variable to access within compareRecordsFunc 
//OrderMaker *sortOrder;

/**
 * @brief Comparator to compare two records based on sortOrder
 * 
 */
class compareRecords{
	OrderMaker *sortOrder;
	public:
	compareRecords(OrderMaker *oMaker){
		sortOrder = oMaker;
	}
	bool operator()(const Record * left,
      const Record * right) const {
      ComparisonEngine ce;
      int val = ce.Compare((Record * ) left, (Record * ) right, sortOrder);
      if (val >= 0) {
        return false;
      }
      return true;
    }
};

/**
 * @brief: Priority Queue used this Comparator for getting the minimum.
 * 
 */
class recordComparisonForPriorityQueue{
	OrderMaker *sortOrder;
	public:
	recordComparisonForPriorityQueue(OrderMaker *oMaker){
		sortOrder = oMaker;
	}
	bool operator()(const pair < Record * , int > pair1,
      const pair < Record * , int > pair2) const {
      ComparisonEngine ce;
      int val = ce.Compare(pair1.first, pair2.first, sortOrder);
      if (val >= 0) {
        return true;
      }
      return false;
    }
};

/**
 * @brief Get the Record's Size 
 * Used to decide if a record will fit icurrent run or a new run needs to be started
 * 
 * @param r : Record 
 * @return int : Size of a record in bits
 */
int GetRecSize(Record * r) {
  char * b = r -> GetBits();
  if (b != NULL) {
    int bits = ((int * ) b)[0];
    return bits;
  }
  return 0;
}

/**
 * @brief Construct a new Big Q:: Big Q object
 * 
 * @param in : Pipe into which the oincoming unsorted records are added
 * @param out : Pipe into which we need to push sorted records
 * @param sortorder : The CNF in which we want to sort the records
 * @param runlen : number of pages in a given run
 */
BigQ :: BigQ (Pipe &inputPipe, Pipe &outputPipe, OrderMaker &sortOrder, int runLength):
	inputPipe(inputPipe),outputPipe(outputPipe),sortOrder(sortOrder),runLength(runLength)
{
	int rc = pthread_create(&workerThread, NULL, processBigQ, (void *) this);
	if(rc)
	{
		printf("Error while creating a thread");
		exit(-1);
	}
}

/**
 * @brief Merges all Runs using a priority queue
 * When merging, adds the records to an output pipe
 * 
 * @param pageIndexesForRun : vector of pairs, which contains the start page number and endPageNumber of each page
 * @param out : pipe to which records need to be pushed
 */
void mergeSortedRuns(vector < pair < int, int > > pageIndexesForRun, Pipe & out, char * fileNameStr, OrderMaker sortOrder) {
	priority_queue < pair < Record * , int > , vector < pair < Record * , int > > , recordComparisonForPriorityQueue > pqRecords(&sortOrder);
  struct Run {
    int currentPageNumber;
    Page curPage;
    Record curRec;
  };

  File fileNew;
  fileNew.Open(1, fileNameStr);
  int numPagesInFile = fileNew.GetLength(), totalRuns = pageIndexesForRun.size(), numRecordsInCurrentRun = 0;
int runNumber = 0;
  Run runs[totalRuns];

  for (int runNumber = 0; runNumber < totalRuns; runNumber++) {
    int startPageNum = pageIndexesForRun[runNumber].first;
    fileNew.GetPage( & (runs[runNumber].curPage), startPageNum);
    (runs[runNumber].curPage).GetFirst( & (runs[runNumber].curRec));
    pqRecords.push(make_pair( & (runs[runNumber].curRec), runNumber));
    runs[runNumber].currentPageNumber = startPageNum;
  }

  while (!pqRecords.empty()) {
    Record * record = pqRecords.top().first;
    int runIdxBelongsTo = pqRecords.top().second;
    pqRecords.pop();

    out.Insert(record);
    runNumber = runIdxBelongsTo;

    int status = (runs[runNumber].curPage).GetFirst( & (runs[runNumber].curRec));
    if (status == 1) {
      pqRecords.push(make_pair( & (runs[runNumber].curRec), runNumber));
    } else {
      int pageNumCurrentRun = (runs[runIdxBelongsTo].currentPageNumber);
      if (pageNumCurrentRun < numPagesInFile - 1) {
        ++pageNumCurrentRun;
        if (!(pageNumCurrentRun > pageIndexesForRun[runIdxBelongsTo].second || pageNumCurrentRun >= numPagesInFile - 1)) {
          (runs[runNumber].currentPageNumber) = pageNumCurrentRun;
          fileNew.GetPage( & (runs[runNumber].curPage), pageNumCurrentRun);
          (runs[runNumber].curPage).GetFirst( & (runs[runNumber].curRec));
          pqRecords.push(make_pair( & (runs[runNumber].curRec), runNumber));
        }
      }
    }
  }
  fileNew.Close();
}

/**
 * @brief this is the main method which is being called from the thread. 
 * The main job is to 
 * 1. read records from the input Pipe 
 * 2. sort the records in to one single run
 * 3. add the sorted records to pages and then pages to a file
 * 4. Merge all Runs using a priority queue
 * 5. push Records into an output pipe.
 * 
 * @return void* 
 */
void * processBigQ(void *arg)
{
	BigQ *obj = (BigQ *)arg;
	Pipe &in = obj->inputPipe;
	Pipe &out = obj->outputPipe;
	OrderMaker *sortOrder = &(obj->sortOrder);

	int runLength = obj->runLength, numRecordsInEachRun[100] = {0}, runNumber = 0;

	vector<Record *> recordsList;
	bool phaseOneOver = false;
	Record *lastRecOfPage = NULL;
	unsigned int runLenSize = PAGE_SIZE * runLength, curSize = 0;
	vector<pair<int,int> >pageIndexesForRun; 

	Record * temp = NULL;
	Page tempPage;
	unsigned int pageCounter = 0 ;
	File oFile;
	stringstream ss;
	ss << rand();
	string ff = "outputBigQ_" + ss.str() + ".bin";
	char * fileNameStr = (char *)ff.c_str(); 

	oFile.Open(0,fileNameStr);

	int totalRecordCount = 0, idxRun = 0;

	while(true){
		while(true){
			temp = new(std::nothrow) Record;
			if (temp != NULL && in .Remove(temp)) {
				curSize = curSize + GetRecSize(temp);

				if (curSize <= runLenSize) {
				recordsList.push_back(temp);
				continue;
				} else {
				break;
				}
			} else {
				phaseOneOver = true;
				temp = NULL;
				break;
			}
		}

		totalRecordCount += recordsList.size();

		//sort vector 
		sort(recordsList.begin(),recordsList.end(),compareRecords(sortOrder));
		
		int startPageNum = pageCounter;
		// Store all the records in file & pages.
		for (int i = 0; i < recordsList.size(); i++) {
			if (!tempPage.Append(recordsList[i])) {
				oFile.AddPage( & tempPage, pageCounter);
				tempPage.EmptyItOut();
				tempPage.Append(recordsList[i]);
				++pageCounter;
			}
		}

		// Add last page 
		oFile.AddPage( & tempPage, pageCounter);
		pageIndexesForRun.push_back(make_pair(startPageNum, pageCounter));
		pageCounter = pageCounter + 1;
		tempPage.EmptyItOut();
		recordsList.clear();
		if (phaseOneOver == true) {
			break;
		}

		recordsList.push_back(temp);
		curSize = GetRecSize(temp);

	}

	oFile.Close();

	mergeSortedRuns(pageIndexesForRun, out, fileNameStr, *sortOrder);
	out.ShutDown();
	return NULL;
}

BigQ::~BigQ () {
}

