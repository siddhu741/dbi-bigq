#include "RelOp.h"
#define RUNLEN 5

struct SFArguments{
	DBFile *dbfile;
	Pipe *outPipe;
	CNF *cnf;
	Record *literal;
};


/**
 * @param inFile DBFile instance to load records from
 * @param outPipe Pipe instance to insert records to
 * @param selOp CNF to filter out records.
 * @param literal Record instance
 */
void SelectFile::Run (DBFile &inFile, Pipe &outPipe, CNF &selOp, Record &literal) {
	SFArguments *args=static_cast<struct SFArguments *>(malloc(sizeof(struct SFArguments)));
	args->dbfile=&inFile;
	args->outPipe=&outPipe;
	args->cnf=&selOp;
	args->literal=&literal;
	int errorCode = pthread_create(&thread, NULL, selectFileWorker, (void *)args);
	if(errorCode  == 0){		
		// cout<< "SelectFile Thread Job Creation Success\n";
	} else {
		cout<<"SelectFile Thread Job creation error: " << errorCode << "\n";
	}
}

void SelectFile::WaitUntilDone() { 
	pthread_join(thread, NULL); 
}


struct SPArguments{
	Pipe *outPipe;
	Pipe *inPipe;
	CNF *cnf;
	Record *literal;
};
/**
 * @param inPipe Pipe instance to read records from
 * @param outPipe Pipe instance to insert records to
 * @param selOp CNF to filter out records.
 * @param literal Record instance
 */
void SelectPipe::Run (Pipe &inPipe, Pipe &outPipe, CNF &selOp, Record &literal){
	SPArguments *args=static_cast<struct SPArguments *>(malloc(sizeof(struct SPArguments)));
	args->inPipe=&inPipe;
	args->outPipe=&outPipe;
	args->cnf=&selOp;
	args->literal=&literal;
	int errorCode = pthread_create(&thread, NULL, selectPipeWorker, (void *)args);
	if(errorCode  == 0){		
		// cout<< "SelectPipe Thread Job Creation Success\n";
	} else {
		cout<<"SelectPipe Thread Job creation error: " << errorCode << "\n";
	}
}

void SelectPipe::WaitUntilDone() { 
	pthread_join(thread, NULL); 
}


struct SumArguments{
	Pipe *outPipe;
	Pipe *inPipe;
	Function *func;
};
/**
 * @param inPipe Pipe instance to read records from
 * @param outPipe Pipe instance to insert sum tuple to
 * @param computeMe function to compute the sum
 */
void Sum::Run (Pipe &inPipe, Pipe &outPipe, Function &computeMe){
	SumArguments *args=static_cast<struct SumArguments *>(malloc(sizeof(struct SumArguments)));
	args->inPipe=&inPipe;
	args->outPipe=&outPipe;
	args->func=&computeMe;
	int errorCode = pthread_create(&thread, NULL, sumWorker, (void *)args);
	if(errorCode  == 0){		
		// cout<< "Sum Thread Job Creation Success\n";
	} else {
		cout<<"Sum Thread Job creation error: " << errorCode << "\n";
	}
}

void Sum::WaitUntilDone() { 
	pthread_join(thread, NULL); 
}


struct DRArguments{
	Pipe *outPipe;
	Pipe *inPipe;
	Schema *mySchema;
};
/**
 * @param inPipe Pipe instance to read records from
 * @param outPipe Pipe instance to insert unique records
 * @param mySchema schema of record.
 */
void DuplicateRemoval::Run (Pipe &inPipe, Pipe &outPipe, Schema &mySchema) {
	DRArguments *args=static_cast<struct DRArguments *>(malloc(sizeof(struct DRArguments)));
	args->inPipe=&inPipe;
	args->outPipe=&outPipe;
	args->mySchema=&mySchema;
	int errorCode = pthread_create(&thread, NULL, duplicateWorker, (void *)args);
	if(errorCode  == 0){		
		// cout<< "DuplicateRemoval Thread Job Creation Success\n";
	} else {
		cout<<"DuplicateRemoval Thread Job creation error: " << errorCode << "\n";
	}
}

void DuplicateRemoval::WaitUntilDone() { 
	pthread_join(thread, NULL); 
}

struct PArguments{
	Pipe *outPipe;
	Pipe *inPipe;
	int *keepMe;
	int numAttsInput;
	int numAttsOutput;
};
/**
 * @param inPipe Pipe instance to read records from
 * @param outPipe Pipe instance to insert records to
 * @param keepMe list of integer values providing the index of attributes to keep in final output
 * @param numAttsInput Number of arribute in input record
 * @param numAttsOutput Number of arribute in final record
 */
void Project::Run (Pipe &inPipe, Pipe &outPipe, int *keepMe, int numAttsInput, int numAttsOutput){
	PArguments *args=static_cast<struct PArguments *>(malloc(sizeof(struct PArguments)));
	args->inPipe=&inPipe;
	args->outPipe=&outPipe;
	args->keepMe=keepMe;
	args->numAttsInput=numAttsInput;
	args->numAttsOutput=numAttsOutput;
	int errorCode = pthread_create(&thread, NULL, projectWorker, (void *)args);
	if(errorCode  == 0){		
		// cout<< "Project Thread Job Creation Success\n";
	} else {
		cout<<"Project Thread Job creation error: " << errorCode << "\n";
	}
}
void Project::WaitUntilDone() { 
	pthread_join(thread, NULL); 
}


struct GBArguments{
	Pipe *outPipe;
	Pipe *inPipe;
	Function *func;
	OrderMaker *groupAtts;
};
/**
 * @param inPipe Pipe instance to read records from
 * @param outPipe Pipe instance to insert records to
 * @param groupAtts Attributes on which we need to perform GroupBy
 * @param computeMe Function to compute sum
 */
 void GroupBy::Run (Pipe &inPipe, Pipe &outPipe, OrderMaker &groupAtts, Function &computeMe) {
	GBArguments *args=static_cast<struct GBArguments *>(malloc(sizeof(struct GBArguments)));
	args->inPipe = &inPipe;
	args->outPipe = &outPipe;
	args->groupAtts = &groupAtts;
	args->func = &computeMe;
	int errorCode = pthread_create(&thread, NULL, groupByWorker, (void *)args);
	if(errorCode  == 0){		
		// cout<< "GroupBy Thread Job Creation Success\n";
	} else {
		cout<<"GroupBy Thread Job creation error: " << errorCode << "\n";
	}
}

void GroupBy::WaitUntilDone() { 
	pthread_join(thread, NULL); 
}


struct WOArguments{
	Pipe *inPipe;
	Schema *mySchema;
	FILE *file;
};
/**
 * @param inPipe Pipe instance to read records from
 * @param outFile File instance to write records
 * @param mySchema Schema of records for written output file 
 */
void WriteOut::Run (Pipe &inPipe, FILE *outFile, Schema &mySchema){
	WOArguments *args=static_cast<struct WOArguments *>(malloc(sizeof(struct WOArguments)));
	args->inPipe = &inPipe;
	args->file = outFile;
	args->mySchema = &mySchema;
	int errorCode = pthread_create(&thread, NULL, writeoutWorker, (void *)args);
	if(errorCode  == 0){		
		// cout<< "WriteOut Thread Job Creation Success\n";
	} else {
		cout<<"WriteOut Thread Job creation error: " << errorCode << "\n";
	}
}

void WriteOut::WaitUntilDone(){
	pthread_join(thread,NULL);
}

struct JArguments{
	Pipe *outPipe;
	Pipe *inPipe,*inPipe2;
	CNF *cnf;
	Record *literal;
};
/**
 * @param inPipeL Pipe instance read records of Table 1
 * @param inPipeR Pipe instance to read records from
 * @param outPipe Pipe instance to insert records to
 * @param selOp CNF to filter out records.
 * @param literal Record instance
 */
void Join::Run (Pipe &inPipeL, Pipe &inPipeR, Pipe &outPipe, CNF &selOp, Record &literal){
	JArguments *args=static_cast<struct JArguments *>(malloc(sizeof(struct JArguments)));
	args->inPipe = &inPipeL;
	args->inPipe2 = &inPipeR;
	args->cnf=&selOp;
	args->literal=&literal;
	args->outPipe=&outPipe;
	int errorCode = pthread_create(&thread, NULL, joinWorker, (void *)args);
	if(errorCode  == 0){		
		// cout<< "Join Thread Job Creation Success\n";
	} else {
		cout<<"Join Thread Job creation error: " << errorCode << "\n";
	}
}

void Join::WaitUntilDone() { 
	pthread_join(thread, NULL); 
}

void *SelectFile::selectFileWorker(void *args){
  	struct SFArguments *arg = (struct SFArguments *)(args);                     
  	arg->dbfile->MoveFirst();
  	Record temoRecord;
  	while(arg->dbfile->GetNext(temoRecord, *(arg->cnf), *(arg->literal)) != 0){
		arg->outPipe->Insert(&temoRecord);
    }
	arg->outPipe->ShutDown();
}

void* SelectPipe::selectPipeWorker(void* args){
	struct SPArguments *arg = (struct SPArguments *)(args);    
	Record tempRecord; 
	ComparisonEngine comp;
	while(arg->inPipe->Remove(&tempRecord))
		if(comp.Compare(&tempRecord, arg->literal, arg->cnf))
			arg->outPipe->Insert(&tempRecord);
	arg->outPipe->ShutDown();
}

void *Sum::sumWorker(void *args){
	struct SumArguments *arg = (struct SumArguments *)(args);   
	Record tempRecord;
	Record *finalRecord = new Record();
	double totalSum = 0.0;
	Attribute DA = {"sum", Double};
	stringstream ss;

	while(arg->inPipe->Remove(&tempRecord)){
		int int_val = 0;
		double dbl_val = 0;
		arg->func->Apply(tempRecord, int_val, dbl_val);
		totalSum += (int_val + dbl_val);
	}
	
	Schema sum_sch ("sum_sch", 1, &DA);
	ss<<totalSum<<"|";
	const char *totalSumStr = ss.str().c_str();
	finalRecord->ComposeRecord(&sum_sch, totalSumStr);
	arg->outPipe->Insert(finalRecord);
	arg->outPipe->ShutDown();
}

void* DuplicateRemoval::duplicateWorker(void* args){
	struct DRArguments *arg = (struct DRArguments *)(args);  
 	OrderMaker sortOrder(arg->mySchema);
  	Pipe sorted(100);
	Record cur, next;
  	ComparisonEngine comp;
  	
	BigQ biq(*arg->inPipe, sorted, sortOrder, RUNLEN);
  	
  	if(sorted.Remove(&cur) == 1) {
    	while(sorted.Remove(&next) == 1)
      		if(comp.Compare(&cur, &next, &sortOrder)) {	
        		arg->outPipe->Insert(&cur);
        		cur.Consume(&next);
      		}
    	arg->outPipe->Insert(&cur);
  	}
  	arg->outPipe->ShutDown();
}


 void* Project::projectWorker(void* args){
	struct PArguments *arg = (struct PArguments *)(args);  
	Record tempRecord;
	int inputAtts = arg->numAttsInput;
	int outputAtts = arg->numAttsOutput;

	while(arg->inPipe->Remove(&tempRecord) == 1) {
		tempRecord.Project(arg->keepMe, outputAtts, inputAtts);
		arg->outPipe->Insert(&tempRecord);
	}
	arg->outPipe->ShutDown();
 }

void* GroupBy::groupByWorker(void* args){
	struct GBArguments *arg = (struct GBArguments *)(args);  
	int numAttsToKeep = arg->groupAtts->numAtts + 1;
	Type type;
	Pipe sortPipe(100);
	Attribute attr;
	attr.name = (char *)"sum";
	attr.myType = type;
	Attribute DA = {"sum", Double};
	Schema *schema = new Schema ((char *)"sum", 1, &DA);
	
	BigQ *bigQ = new BigQ(*(arg->inPipe), sortPipe, *(arg->groupAtts), RUNLEN);
	
	int ir;  double dr;
	
	int *attsToKeep = new int[numAttsToKeep];
	attsToKeep[0] = 0;

	int i = 1;
	while (i < numAttsToKeep){
		attsToKeep[i] = arg->groupAtts->whichAtts[i-1];
		i = i + 1;
	} 																																																																																																																																															
	ComparisonEngine comp;
	Record *tmpRcd = new Record();
	if(sortPipe.Remove(tmpRcd) != 1){

	} else{
		bool processFurther = true;
		while(processFurther) {
			processFurther = false;
			type = arg->func->Apply(*tmpRcd, ir, dr);
			double sum = 0;
			sum += (ir + dr);
			Record *r = new Record();
			Record *lastRcd = new Record;
			lastRcd->Copy(tmpRcd);
			while(sortPipe.Remove(r) == 1) {
				if(comp.Compare(lastRcd, r, arg->groupAtts)){
					tmpRcd->Copy(r);
					processFurther = true;
					break;
				} else {
					type = arg->func->Apply(*r, ir, dr);
					sum += (ir+dr);
				}
			}
			ostringstream ss;
			ss << sum << "|";
			const char *sumStr = ss.str().c_str();
			Record *sumRcd = new Record();
			sumRcd->ComposeRecord(schema, sumStr);

			Record *tuple = new Record;
			tuple->MergeRecords(sumRcd, lastRcd, 1, arg->groupAtts->numAtts, attsToKeep,  numAttsToKeep, 1);
			arg->outPipe->Insert(tuple);
		}
	}
	arg->outPipe->ShutDown();
}

void* WriteOut::writeoutWorker(void* args){
		struct WOArguments *arg = (struct WOArguments *)(args); 
		Attribute *atts = arg->mySchema->GetAtts();
		int n = arg->mySchema->GetNumAtts();
		Record rec;
		int cnt=1;
		while(arg->inPipe->Remove(&rec) == 1){
			if(arg->file){	
				fprintf(arg->file, "%d.) ", cnt++);
				char *bits = rec.bits;
				for (int i = 0; i < n; i++) {
					fprintf(arg->file, "type: %s ", atts[i].name);
					int pointer = ((int *) bits)[i + 1];
					fprintf(arg->file, "value: ");
					if (atts[i].myType == String) {
						char *myString = (char *) &(bits[pointer]);
						fprintf(arg->file, "%s", myString);
					} else if (atts[i].myType == Double) {
						double *myDouble = (double *) &(bits[pointer]);
						fprintf(arg->file, "%f", *myDouble);
					} else if (atts[i].myType == Int) {
						int *myInt = (int *) &(bits[pointer]);
						fprintf(arg->file, "%d", *myInt);
					} 
					if (i != n - 1) {
						fprintf(arg->file, "& ");
					}
				}
				fprintf(arg->file, "\n");
			} else{
				std::cout<<"Row "<<cnt++<<"\n";
				rec.Print(arg->mySchema);
			}
		}
		cout << "total number of records are: " << cnt - 1 << endl;
		if(arg->file){
			fclose(arg->file);
		}
}


void* Join::joinWorker(void* args){
	try{
		struct JArguments *arg = (struct JArguments *)(args); 
		OrderMaker orderL;
		OrderMaker orderR;
		arg->cnf->GetSortOrders(orderL, orderR);

		Record *rcdLeft = new Record();
		Record *rcdRight = new Record();
		ComparisonEngine comp;

		if(orderL.numAtts && orderR.numAtts && orderL.numAtts == orderR.numAtts) {
			Pipe *pipeL=new Pipe(100), *pipeR=new Pipe(100);
			BigQ *bigQL = new BigQ(*(arg->inPipe), *pipeL, orderL, RUNLEN);
			BigQ *bigQR = new BigQ(*(arg->inPipe2), *pipeR, orderR, RUNLEN);
			
			vector<Record *> leftRecords;
			vector<Record *> rightRecords;
			
			if(pipeL->Remove(rcdLeft) && pipeR->Remove(rcdRight)) {
				int lAttr = ((int *) rcdLeft->bits)[1] / sizeof(int) -1;
				int rAttr = ((int *) rcdRight->bits)[1] / sizeof(int) -1;
				int attrToKeep[lAttr + rAttr];

				int i = 0, joinNum, num = 0;
				while (i < lAttr){
					attrToKeep[i] = i;
					i ++;
				}
				i = 0;
				while (i < rAttr){
					attrToKeep[i + lAttr] = i;
					i ++;
				}

				bool leftGood = true, rightGood = true;
				while(leftGood && rightGood) {
					leftGood=false; rightGood=false;
					int cmpRst = comp.Compare(rcdLeft, &orderL, rcdRight, &orderR);
					if (cmpRst == 0){
						num ++;
						Record *rcd1 = new Record(); 
						rcd1->Consume(rcdLeft);

						Record *rcd2 = new Record(); 
						rcd2->Consume(rcdRight);

						leftRecords.push_back(rcd1);
						rightRecords.push_back(rcd2);
						while(pipeL->Remove(rcdLeft) == 1) {
							if(comp.Compare(rcdLeft, rcd1, &orderL)){
								leftGood = true;
								break;
							} else{
								Record *cLMe = new Record();
								cLMe->Consume(rcdLeft);
								leftRecords.push_back(cLMe);
							}
						}
						while(pipeR->Remove(rcdRight) == 1) {
							if(comp.Compare(rcdRight, rcd2, &orderR)){
								rightGood = true;
								break;
							}else {
								Record *cRMe = new Record();
								cRMe->Consume(rcdRight);
								rightRecords.push_back(cRMe);
							} 
						}
						Record *lr = new Record();
						Record *rr=new Record();
						Record *jr = new Record();
						for(auto itL :leftRecords) {
							lr->Consume(itL);
							for(auto itR: rightRecords) {
								if(comp.Compare(lr, itR, arg->literal, arg->cnf)) {
									joinNum++;
									rr->Copy(itR);
									jr->MergeRecords(lr, rr, lAttr, rAttr, attrToKeep, lAttr+rAttr, lAttr);
									arg->outPipe->Insert(jr);
								}
							}
						}
						for(auto it:leftRecords){
							if(!it){
								delete it; 
							}
						}
						leftRecords.clear();
						for(auto it : rightRecords){
							if(!it){
								delete it;
							}
						}
						rightRecords.clear();
					}else if(cmpRst == 1){
						leftGood = true;
						if(pipeR->Remove(rcdRight)){
							rightGood = true;
						}
					}else if(cmpRst == -1){
						rightGood = true;
						if(pipeL->Remove(rcdLeft)){
							leftGood = true;
						}
					}
				}
			}
		}else {
			int n_pages = 10;
			
			Page pageR;
			DBFile dbFileL;
			fType ft = heap;
			dbFileL.Create((char*)"tmpL", ft, NULL);
			dbFileL.MoveFirst();

			int leftAttr, rightAttr, *attrToKeep, joinNum = 0;

			if(arg->inPipe->Remove(rcdLeft) && arg->inPipe2->Remove(rcdRight)) {
				leftAttr = ((int *) rcdLeft->bits)[1] / sizeof(int) -1;
				rightAttr = ((int *) rcdRight->bits)[1] / sizeof(int) -1;
				attrToKeep = new int[leftAttr + rightAttr];
				int i = 0;
				while (i < leftAttr){
					attrToKeep[i] = i;
					i ++;
				}
				i = 0;
				while (i < rightAttr){
					attrToKeep[i + leftAttr] = i;
					i ++;
				}
				do {
					dbFileL.Add(*rcdLeft);
				}while(arg->inPipe->Remove(rcdLeft));
				vector<Record *> vecR;

				bool continueProcessing = true;
				while(continueProcessing) {
					Record *first = new Record();
					first->Copy(rcdRight);
					pageR.Append(rcdRight);
					vecR.push_back(first);
					int rPages = 0;
					continueProcessing = false;
					while(arg->inPipe2->Remove(rcdRight) == 1) {
						Record *copyMe = new Record();
						copyMe->Copy(rcdRight);
						if(!pageR.Append(rcdRight)) {
							rPages += 1;
							if(rPages >= n_pages -1) {
								continueProcessing = true;
								break;
							}else {
								pageR.EmptyItOut();
								pageR.Append(rcdRight);
								vecR.push_back(copyMe);
							}
						} else {
							vecR.push_back(copyMe);
						}
					}
					dbFileL.MoveFirst();
					int fileRN = 0;
					while(dbFileL.GetNext(*rcdLeft)) {
						for(auto it:vecR) {
							if(comp.Compare(rcdLeft, it, arg->literal, arg->cnf)) {
								joinNum++;
								Record *joinRec = new Record();
								Record *rightRec = new Record();
								rightRec->Copy(it);
								joinRec->MergeRecords(rcdLeft, rightRec, leftAttr, rightAttr, attrToKeep, leftAttr+rightAttr, leftAttr);
								arg->outPipe->Insert(joinRec);
							}
						}
					}
					for(auto it : vecR)
						if(!it)
							delete it;
					vecR.clear();
				}
				dbFileL.Close();
			}
		}
		arg->outPipe->ShutDown();
	}
	catch(std::exception e){
		std::cout<<"Exception in Join Thread\n";
	}
 }

